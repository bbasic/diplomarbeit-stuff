/**
 */
package petrinet;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Transition</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see petrinet.PetrinetPackage#getTransition()
 * @model annotation="gmf.node label.icon='false' label='name' figure='rectangle' color='0,0,0' label.placement='external' size='40,15'"
 * @generated
 */
public interface Transition extends Node
{

} // Transition
