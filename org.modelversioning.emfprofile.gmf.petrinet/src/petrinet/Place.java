/**
 */
package petrinet;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Place</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see petrinet.PetrinetPackage#getPlace()
 * @model annotation="gmf.node label='name' label.icon='false' figure='ellipse' border.width='2' border.style='solid' label.placement='external' size='40,40' color='20,20,20'"
 * @generated
 */
public interface Place extends Node
{
} // Place
