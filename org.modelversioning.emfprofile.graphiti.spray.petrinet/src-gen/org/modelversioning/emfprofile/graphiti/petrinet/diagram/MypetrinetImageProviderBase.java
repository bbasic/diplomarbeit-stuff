/*************************************************************************************
 *
 * Generated on Thu Sep 04 16:12:39 CEST 2014 by Spray ImageProvider.xtend
 *
 * This file contains generated and should not be changed.
 * Use the extension point class (the direct subclass of this class) to add manual code
 *
 *************************************************************************************/
package org.modelversioning.emfprofile.graphiti.petrinet.diagram;

import org.eclipse.graphiti.ui.platform.AbstractImageProvider;

public abstract class MypetrinetImageProviderBase extends AbstractImageProvider {
    // The prefix for all identifiers of this image provider
    public static final String PREFIX = "org.modelversioning.emfprofile.graphiti.petrinet.diagram.";

    /**
     * {@inheritDoc}
     */
    @Override
    protected void addAvailableImages() {
        // register the path for each image identifier
    }
}
