/*************************************************************************************
 *
 * Generated on Thu Sep 04 16:12:39 CEST 2014 by Spray ToolBehaviorProvider.xtend
 *
 * This file contains generated and should not be changed.
 * Use the extension point class (the direct subclass of this class) to add manual code
 *
 *************************************************************************************/
package org.modelversioning.emfprofile.graphiti.petrinet.diagram;

import org.eclipse.graphiti.dt.IDiagramTypeProvider;
import org.eclipse.graphiti.features.IFeature;
import org.eclipse.graphiti.palette.IPaletteCompartmentEntry;
import org.eclipselabs.spray.runtime.graphiti.layout.SprayLayoutService;
import org.eclipselabs.spray.runtime.graphiti.tb.AbstractSprayToolBehaviorProvider;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.emf.common.util.EList;
import org.eclipse.graphiti.services.Graphiti;

import com.google.common.collect.Lists;
import org.modelversioning.emfprofile.graphiti.petrinet.features.MypetrinetCreateInputArcFeature;
import org.modelversioning.emfprofile.graphiti.petrinet.features.MypetrinetCreateOutputArcFeature;
import org.modelversioning.emfprofile.graphiti.petrinet.features.MypetrinetCreatePlaceFeature;
import org.modelversioning.emfprofile.graphiti.petrinet.features.MypetrinetCreateTransitionFeature;

public abstract class MypetrinetToolBehaviorProviderBase extends AbstractSprayToolBehaviorProvider {
    protected static final String COMPARTMENT_ELEMENTS = "Elements";
    protected static final String COMPARTMENT_ARCS     = "Arcs";

    public MypetrinetToolBehaviorProviderBase(final IDiagramTypeProvider dtp) {
        super(dtp);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public GraphicsAlgorithm getSelectionBorder(PictogramElement pe) {
        boolean isFromDsl = SprayLayoutService.isShapeFromDsl(pe);
        if (isFromDsl) {
            ContainerShape container = (ContainerShape) pe;
            if (!container.getChildren().isEmpty()) {
                return container.getChildren().get(0).getGraphicsAlgorithm();
            }
        }
        return super.getSelectionBorder(pe);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void buildCreationTools() {
        buildCreationTool(new MypetrinetCreatePlaceFeature(this.getFeatureProvider()));
        buildCreationTool(new MypetrinetCreateTransitionFeature(this.getFeatureProvider()));
        buildCreationTool(new MypetrinetCreateOutputArcFeature(this.getFeatureProvider()));
        buildCreationTool(new MypetrinetCreateInputArcFeature(this.getFeatureProvider()));
        // Compartments
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Iterable<IPaletteCompartmentEntry> buildPaletteCompartments() {
        return Lists.newArrayList(getPaletteCompartment(COMPARTMENT_ELEMENTS), getPaletteCompartment(COMPARTMENT_ARCS), getPaletteCompartment(COMPARTMENT_DEFAULT));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected IPaletteCompartmentEntry getPaletteCompartmentForFeature(final IFeature feature) {
        if (feature instanceof MypetrinetCreatePlaceFeature) {
            return getPaletteCompartment(COMPARTMENT_ELEMENTS);
        } else if (feature instanceof MypetrinetCreateTransitionFeature) {
            return getPaletteCompartment(COMPARTMENT_ELEMENTS);
        } else if (feature instanceof MypetrinetCreateOutputArcFeature) {
            return getPaletteCompartment(COMPARTMENT_ARCS);
        } else if (feature instanceof MypetrinetCreateInputArcFeature) {
            return getPaletteCompartment(COMPARTMENT_ARCS);
        }
        return super.getPaletteCompartmentForFeature(feature);
    }
}
