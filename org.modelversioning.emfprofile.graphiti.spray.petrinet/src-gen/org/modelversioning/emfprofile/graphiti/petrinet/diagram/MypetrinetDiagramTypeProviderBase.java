/*************************************************************************************
 *
 * Generated on Thu Sep 04 16:12:39 CEST 2014 by Spray DiagramTypeProvider.xtend
 *
 * This file contains generated and should not be changed.
 * Use the extension point class (the direct subclass of this class) to add manual code
 *
 *************************************************************************************/
package org.modelversioning.emfprofile.graphiti.petrinet.diagram;

import org.eclipse.graphiti.dt.AbstractDiagramTypeProvider;
import org.eclipse.graphiti.tb.IToolBehaviorProvider;

public abstract class MypetrinetDiagramTypeProviderBase extends AbstractDiagramTypeProvider {
    protected IToolBehaviorProvider[] toolBehaviorProviders;

    public MypetrinetDiagramTypeProviderBase() {
        super();
        setFeatureProvider(new MypetrinetFeatureProvider(this));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IToolBehaviorProvider[] getAvailableToolBehaviorProviders() {
        if (toolBehaviorProviders == null) {
            toolBehaviorProviders = new IToolBehaviorProvider[]{new MypetrinetToolBehaviorProvider(this)};
        }
        return toolBehaviorProviders;
    }

}
