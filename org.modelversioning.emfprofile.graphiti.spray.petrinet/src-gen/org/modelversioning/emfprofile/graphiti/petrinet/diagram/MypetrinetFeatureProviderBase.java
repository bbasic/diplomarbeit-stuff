/*************************************************************************************
 *
 * Generated on Thu Sep 04 16:12:39 CEST 2014 by Spray FeatureProvider.xtend
 *
 * This file contains generated and should not be changed.
 * Use the extension point class (the direct subclass of this class) to add manual code
 *
 *************************************************************************************/
package org.modelversioning.emfprofile.graphiti.petrinet.diagram;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.dt.IDiagramTypeProvider;
import org.eclipse.graphiti.features.IAddFeature;
import org.eclipse.graphiti.features.ICreateConnectionFeature;
import org.eclipse.graphiti.features.ICreateFeature;
import org.eclipse.graphiti.features.ICopyFeature;
import org.eclipse.graphiti.features.ILayoutFeature;
import org.eclipse.graphiti.features.IMoveShapeFeature;
import org.eclipse.graphiti.features.IPasteFeature;
import org.eclipse.graphiti.features.IUpdateFeature;
import org.eclipse.graphiti.features.IDeleteFeature;
import org.eclipse.graphiti.features.IDirectEditingFeature;
import org.eclipse.graphiti.features.IRemoveFeature;
import org.eclipse.graphiti.features.IResizeShapeFeature;
import org.eclipse.graphiti.features.context.IAddContext;
import org.eclipse.graphiti.features.context.ICustomContext;
import org.eclipse.graphiti.features.context.ICopyContext;
import org.eclipse.graphiti.features.context.IDeleteContext;
import org.eclipse.graphiti.features.context.IDirectEditingContext;
import org.eclipse.graphiti.features.context.ILayoutContext;
import org.eclipse.graphiti.features.context.IMoveShapeContext;
import org.eclipse.graphiti.features.context.IUpdateContext;
import org.eclipse.graphiti.features.context.IPasteContext;
import org.eclipse.graphiti.features.context.IRemoveContext;
import org.eclipse.graphiti.features.context.IResizeShapeContext;
import org.eclipse.graphiti.features.custom.ICustomFeature;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.mm.pictograms.Shape;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;

import org.eclipselabs.spray.runtime.graphiti.GraphitiProperties;
import org.eclipselabs.spray.runtime.graphiti.features.DefaultDeleteFeature;
import org.eclipselabs.spray.runtime.graphiti.features.DefaultFeatureProvider;
import org.eclipselabs.spray.runtime.graphiti.features.DefaultRemoveFeature;
import org.eclipselabs.spray.runtime.graphiti.layout.SprayLayoutService;
import org.eclipselabs.spray.runtime.graphiti.layout.SprayFixedLayoutManager;
import org.eclipselabs.spray.runtime.graphiti.containers.OwnerPropertyDeleteFeature;
import org.modelversioning.emfprofile.graphiti.petrinet.features.MypetrinetAddInputArcFeature;
import org.modelversioning.emfprofile.graphiti.petrinet.features.MypetrinetAddOutputArcFeature;
import org.modelversioning.emfprofile.graphiti.petrinet.features.MypetrinetAddPlaceFeature;
import org.modelversioning.emfprofile.graphiti.petrinet.features.MypetrinetAddTransitionFeature;
import org.modelversioning.emfprofile.graphiti.petrinet.features.MypetrinetCopyFeature;
import org.modelversioning.emfprofile.graphiti.petrinet.features.MypetrinetCreateInputArcFeature;
import org.modelversioning.emfprofile.graphiti.petrinet.features.MypetrinetCreateOutputArcFeature;
import org.modelversioning.emfprofile.graphiti.petrinet.features.MypetrinetCreatePlaceFeature;
import org.modelversioning.emfprofile.graphiti.petrinet.features.MypetrinetCreateTransitionFeature;
import org.modelversioning.emfprofile.graphiti.petrinet.features.MypetrinetDirectEditInputArcFeature;
import org.modelversioning.emfprofile.graphiti.petrinet.features.MypetrinetDirectEditOutputArcFeature;
import org.modelversioning.emfprofile.graphiti.petrinet.features.MypetrinetDirectEditPlaceFeature;
import org.modelversioning.emfprofile.graphiti.petrinet.features.MypetrinetDirectEditTransitionFeature;
import org.modelversioning.emfprofile.graphiti.petrinet.features.MypetrinetLayoutPlaceFeature;
import org.modelversioning.emfprofile.graphiti.petrinet.features.MypetrinetLayoutTransitionFeature;
import org.modelversioning.emfprofile.graphiti.petrinet.features.MypetrinetPasteFeature;
import org.modelversioning.emfprofile.graphiti.petrinet.features.MypetrinetResizePlaceFeature;
import org.modelversioning.emfprofile.graphiti.petrinet.features.MypetrinetResizeTransitionFeature;
import org.modelversioning.emfprofile.graphiti.petrinet.features.MypetrinetUpdateInputArcFeature;
import org.modelversioning.emfprofile.graphiti.petrinet.features.MypetrinetUpdateOutputArcFeature;
import org.modelversioning.emfprofile.graphiti.petrinet.features.MypetrinetUpdatePlaceFeature;
import org.modelversioning.emfprofile.graphiti.petrinet.features.MypetrinetUpdateTransitionFeature;
import petrinet.PetrinetPackage;

@SuppressWarnings("unused")
public abstract class MypetrinetFeatureProviderBase extends DefaultFeatureProvider {
    public MypetrinetFeatureProviderBase(final IDiagramTypeProvider dtp) {
        super(dtp);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IAddFeature getAddFeature(final IAddContext context) {
        // is object for add request a EClass or EReference?
        final EObject bo = (EObject) context.getNewObject();
        final String reference = (String) context.getProperty(PROPERTY_REFERENCE);
        final String alias = (String) context.getProperty(PROPERTY_ALIAS);
        if (bo.eClass() == PetrinetPackage.Literals.PLACE && alias == null) {
            if (reference == null) {
                return new MypetrinetAddPlaceFeature(this);
            }
        }
        if (bo.eClass() == PetrinetPackage.Literals.TRANSITION && alias == null) {
            if (reference == null) {
                return new MypetrinetAddTransitionFeature(this);
            }
        }
        if (bo.eClass() == PetrinetPackage.Literals.OUTPUT_ARC && alias == null) {
            if (reference == null) {
                return new MypetrinetAddOutputArcFeature(this);
            }
        }
        if (bo.eClass() == PetrinetPackage.Literals.INPUT_ARC && alias == null) {
            if (reference == null) {
                return new MypetrinetAddInputArcFeature(this);
            }
        }
        return super.getAddFeature(context);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ICopyFeature getCopyFeature(ICopyContext context) {
        return new MypetrinetCopyFeature(this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ICreateFeature[] getCreateFeatures() {
        return new ICreateFeature[]{new MypetrinetCreatePlaceFeature(this), new MypetrinetCreateTransitionFeature(this)};
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ICreateConnectionFeature[] getCreateConnectionFeatures() {
        return new ICreateConnectionFeature[]{new MypetrinetCreateOutputArcFeature(this), new MypetrinetCreateInputArcFeature(this)};
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IUpdateFeature getUpdateFeature(final IUpdateContext context) {
        final PictogramElement pictogramElement = context.getPictogramElement();
        String alias;
        if (pictogramElement instanceof Shape) {
            Shape dslShape = SprayLayoutService.findDslShape(pictogramElement);
            alias = peService.getPropertyValue(dslShape, PROPERTY_ALIAS);
        } else {
            alias = peService.getPropertyValue(pictogramElement, PROPERTY_ALIAS);
        }
        //    if (pictogramElement instanceof ContainerShape) {
        final EObject bo = (EObject) getBusinessObjectForPictogramElement(pictogramElement);
        if (bo == null) {
            return null;
        }
        if (bo.eClass() == PetrinetPackage.Literals.PLACE && alias == null) { // 11
            return new MypetrinetUpdatePlaceFeature(this);
        }
        if (bo.eClass() == PetrinetPackage.Literals.TRANSITION && alias == null) { // 11
            return new MypetrinetUpdateTransitionFeature(this);
        }
        if (bo instanceof petrinet.OutputArc && alias == null) { // 33
            return new MypetrinetUpdateOutputArcFeature(this);
        }
        if (bo instanceof petrinet.InputArc && alias == null) { // 33
            return new MypetrinetUpdateInputArcFeature(this);
        }
        //        }
        return super.getUpdateFeature(context);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ILayoutFeature getLayoutFeature(final ILayoutContext context) {
        final PictogramElement pictogramElement = context.getPictogramElement();
        final EObject bo = (EObject) getBusinessObjectForPictogramElement(pictogramElement);
        if (bo == null) {
            return null;
        }
        final String alias = peService.getPropertyValue(pictogramElement, PROPERTY_ALIAS);
        if (bo.eClass() == PetrinetPackage.Literals.PLACE && alias == null) {
            return new MypetrinetLayoutPlaceFeature(this);
        }
        if (bo.eClass() == PetrinetPackage.Literals.TRANSITION && alias == null) {
            return new MypetrinetLayoutTransitionFeature(this);
        }
        return super.getLayoutFeature(context);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IRemoveFeature getRemoveFeature(final IRemoveContext context) {
        // Spray specific DefaultRemoveFeature
        final PictogramElement pictogramElement = context.getPictogramElement();
        return new DefaultRemoveFeature(this);
    }

    public IDeleteFeature getDeleteFeature(final IDeleteContext context) {
        final PictogramElement pictogramElement = context.getPictogramElement();
        final EObject bo = getBusinessObjectForPictogramElement(pictogramElement);
        if (bo == null) {
            return null;
        }
        final String reference = peService.getPropertyValue(pictogramElement, PROPERTY_REFERENCE);
        final String alias = peService.getPropertyValue(pictogramElement, PROPERTY_ALIAS);

        if (bo.eClass() == PetrinetPackage.Literals.PLACE && alias == null) {
            if (reference == null) {
                return new DefaultDeleteFeature(this);
            }
        }
        if (bo.eClass() == PetrinetPackage.Literals.TRANSITION && alias == null) {
            if (reference == null) {
                return new DefaultDeleteFeature(this);
            }
        }
        if (bo.eClass() == PetrinetPackage.Literals.OUTPUT_ARC && alias == null) {
            if (reference == null) {
                return new DefaultDeleteFeature(this);
            }
        }
        if (bo.eClass() == PetrinetPackage.Literals.INPUT_ARC && alias == null) {
            if (reference == null) {
                return new DefaultDeleteFeature(this);
            }
        }

        return new DefaultDeleteFeature(this);
    }

    /** 
     * Ensure that any shape with property {@link ISprayConstants#CAN_MOVE} set to false will not have a move feature.
     */
    /**
     * {@inheritDoc}
     */
    @Override
    public IMoveShapeFeature getMoveShapeFeature(final IMoveShapeContext context) {
        final Shape shape = context.getShape();
        final ContainerShape parent = shape.getContainer();
        EObject eObject = getBusinessObjectForPictogramElement(shape);
        ContainerShape targetContainer = context.getTargetContainer();
        EObject target = getBusinessObjectForPictogramElement(targetContainer);
        if (eObject instanceof petrinet.Place) {
            return new org.modelversioning.emfprofile.graphiti.petrinet.features.MypetrinetMovePlaceFeature(this);
        }

        if (eObject instanceof petrinet.Transition) {
            return new org.modelversioning.emfprofile.graphiti.petrinet.features.MypetrinetMoveTransitionFeature(this);
        }

        return super.getMoveShapeFeature(context);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IPasteFeature getPasteFeature(IPasteContext context) {
        return new MypetrinetPasteFeature(this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IDirectEditingFeature getDirectEditingFeature(IDirectEditingContext context) {
        final PictogramElement pictogramElement = context.getPictogramElement();
        final EObject bo = (EObject) getBusinessObjectForPictogramElement(pictogramElement);
        if (bo == null) {
            return null;
        }
        if (SprayLayoutService.isCompartment(pictogramElement)) {
            return null;
        }
        String alias = null;
        if (pictogramElement instanceof Shape) {
            Shape dslShape = SprayLayoutService.findDslShape(pictogramElement);
            alias = peService.getPropertyValue(dslShape, PROPERTY_ALIAS);
        } else {
            alias = peService.getPropertyValue(pictogramElement, PROPERTY_ALIAS);
        }
        if (bo.eClass() == PetrinetPackage.Literals.PLACE && alias == null) {
            return new MypetrinetDirectEditPlaceFeature(this);
        }
        if (bo.eClass() == PetrinetPackage.Literals.TRANSITION && alias == null) {
            return new MypetrinetDirectEditTransitionFeature(this);
        }
        if (bo.eClass() == PetrinetPackage.Literals.OUTPUT_ARC && alias == null) {
            return new MypetrinetDirectEditOutputArcFeature(this);
        }
        if (bo.eClass() == PetrinetPackage.Literals.INPUT_ARC && alias == null) {
            return new MypetrinetDirectEditInputArcFeature(this);
        }
        return super.getDirectEditingFeature(context);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ICustomFeature[] getCustomFeatures(final ICustomContext context) {
        final EObject bo = (EObject) getBusinessObjectForPictogramElement(context.getPictogramElements()[0]);
        if (bo == null) {
            return new ICustomFeature[0];
        }
        final String alias = GraphitiProperties.get(context.getPictogramElements()[0], PROPERTY_ALIAS);
        return new ICustomFeature[0];
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IResizeShapeFeature getResizeShapeFeature(IResizeShapeContext context) {
        final PictogramElement pictogramElement = context.getPictogramElement();
        final EObject bo = (EObject) getBusinessObjectForPictogramElement(pictogramElement);
        if (bo == null) {
            return null;
        }
        final String alias = peService.getPropertyValue(pictogramElement, PROPERTY_ALIAS);
        if (bo.eClass() == PetrinetPackage.Literals.PLACE && alias == null) {
            return new MypetrinetResizePlaceFeature(this);
        }
        if (bo.eClass() == PetrinetPackage.Literals.TRANSITION && alias == null) {
            return new MypetrinetResizeTransitionFeature(this);
        }
        return super.getResizeShapeFeature(context);
    }
}
