/*************************************************************************************
 *
 * Generated on Thu Sep 04 16:12:39 CEST 2014 by Spray ExecutableExtensionFactory.xtend
 *
 * This file contains generated and should not be changed.
 * Use the extension point class (the direct subclass of this class) to add manual code
 *
 *************************************************************************************/
package org.modelversioning.emfprofile.graphiti.petrinet.internal;

import com.google.inject.Injector;
import org.eclipse.xtext.ui.guice.AbstractGuiceAwareExecutableExtensionFactory;
import org.osgi.framework.Bundle;
import org.modelversioning.emfprofile.graphiti.petrinet.Activator;

public class ExecutableExtensionFactory extends AbstractGuiceAwareExecutableExtensionFactory {

    /**
     * {@inheritDoc}
     */
    @Override
    protected Bundle getBundle() {
        return Activator.getDefault().getBundle();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Injector getInjector() {
        return Activator.getDefault().getInjector();
    }

}
