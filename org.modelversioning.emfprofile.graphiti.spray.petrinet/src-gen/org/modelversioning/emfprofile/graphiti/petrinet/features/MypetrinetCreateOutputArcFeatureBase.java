/*************************************************************************************
 *
 * Generated on Thu Sep 04 16:12:39 CEST 2014 by Spray CreateConnectionFeature.xtend
 *
 * This file contains generated and should not be changed.
 * Use the extension point class (the direct subclass of this class) to add manual code
 *
 *************************************************************************************/
package org.modelversioning.emfprofile.graphiti.petrinet.features;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.ICreateConnectionContext;
import org.eclipse.graphiti.features.context.impl.AddConnectionContext;
import org.eclipse.graphiti.mm.pictograms.Anchor;
import org.eclipse.graphiti.mm.pictograms.Connection;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.mm.pictograms.Shape;
import org.eclipse.graphiti.services.IGaService;
import org.eclipselabs.spray.runtime.graphiti.features.AbstractCreateConnectionFeature;
import org.eclipselabs.spray.runtime.graphiti.containers.SampleUtil;
import org.eclipselabs.spray.runtime.graphiti.layout.SprayLayoutService;
import org.modelversioning.emfprofile.graphiti.petrinet.Activator;
import petrinet.PetrinetFactory;

public abstract class MypetrinetCreateOutputArcFeatureBase extends AbstractCreateConnectionFeature {
    protected static String TITLE         = "Create OutputArc";
    protected static String USER_QUESTION = "Enter new OutputArc name";

    public MypetrinetCreateOutputArcFeatureBase(final IFeatureProvider fp) {
        // provide name and description for the UI, e.g. the palette
        super(fp, "OutputArc", "Create OutputArc");
        gaService = Activator.get(IGaService.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canCreate(final ICreateConnectionContext context) {
        Anchor targetAnchor = getDslShapeAnchor(context.getTargetPictogramElement());
        if (targetAnchor == null) {
            return false;
        }
        // return true if both anchors belong to an EClass of the right type and those EClasses are not identical
        Anchor sourceAnchor = getDslShapeAnchor(context.getSourcePictogramElement());
        petrinet.Transition source = getTransition(sourceAnchor);
        petrinet.Place target = getPlace(targetAnchor);
        if ((source != null) && (target != null) && (source != target)) {
            return true;
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canStartConnection(final ICreateConnectionContext context) {
        // return true if start anchor belongs to a EClass of the right type
        Anchor sourceAnchor = getDslShapeAnchor(context.getSourcePictogramElement());
        if (getTransition(sourceAnchor) != null) {
            return true;
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Connection create(final ICreateConnectionContext context) {
        Connection newConnection = null;
        Anchor sourceAnchor = getDslShapeAnchor(context.getSourcePictogramElement());
        Anchor targetAnchor = getDslShapeAnchor(context.getTargetPictogramElement());

        // get EClasses which should be connected
        final petrinet.Transition source = getTransition(sourceAnchor);
        final petrinet.Place target = getPlace(targetAnchor);
        // containment reference is not a feature of source
        final petrinet.PetriNet container = org.eclipse.xtext.EcoreUtil2.getContainerOfType(source, petrinet.PetriNet.class);
        if (source != null && target != null) {
            // create new business object
            final petrinet.OutputArc eReference = createOutputArc(source, target);
            // add the element to containment reference
            container.getElements().add(eReference);
            // add connection for business object
            final AddConnectionContext addContext = new AddConnectionContext(sourceAnchor, targetAnchor);
            addContext.setNewObject(eReference);
            newConnection = (Connection) getFeatureProvider().addIfPossible(addContext);

            // activate direct editing after object creation
            getFeatureProvider().getDirectEditingInfo().setActive(true);
        }

        return newConnection;
    }

    /**
     * Returns the Transition belonging to the anchor, or <code>null</code> if not available.
     */
    protected petrinet.Transition getTransition(final Anchor anchor) {
        if (anchor != null) {
            final EObject bo = (EObject) getBusinessObjectForPictogramElement(anchor.getParent());
            if (bo instanceof petrinet.Transition) {
                return (petrinet.Transition) bo;
            }
        }
        return null;
    }

    /**
     * Returns the Place belonging to the anchor, or <code>null</code> if not available.
     */
    protected petrinet.Place getPlace(final Anchor anchor) {
        if (anchor != null) {
            final EObject bo = (EObject) getBusinessObjectForPictogramElement(anchor.getParent());
            if (bo instanceof petrinet.Place) {
                return (petrinet.Place) bo;
            }
        }
        return null;
    }

    /**
     * Creates a EReference between two EClasses.
     */
    protected petrinet.OutputArc createOutputArc(final petrinet.Transition source, final petrinet.Place target) {
        // TODO: Domain Object
        final petrinet.OutputArc domainObject = PetrinetFactory.eINSTANCE.createOutputArc();
        domainObject.setFrom(source);
        domainObject.setTo(target);

        setDoneChanges(true);
        return domainObject;
    }

    protected Anchor getDslShapeAnchor(PictogramElement pe) {
        if (pe == null) {
            return null;
        }
        Shape dslShape = SprayLayoutService.findDslShape(pe);
        if (dslShape != null) {
            EList<Anchor> anchors = dslShape.getAnchors();
            if (!anchors.isEmpty()) {
                return anchors.get(0);
            }
        }
        return null;
    }
}
