/*************************************************************************************
 *
 * Generated on Thu Sep 04 16:12:39 CEST 2014 by Spray AddConnectionFromDslFeature.xtend
 *
 * This file contains generated and should not be changed.
 * Use the extension point class (the direct subclass of this class) to add manual code
 *
 *************************************************************************************/
package org.modelversioning.emfprofile.graphiti.petrinet.features;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IAddConnectionContext;
import org.eclipse.graphiti.features.context.IAddContext;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.pictograms.Connection;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.mm.pictograms.ConnectionDecorator;
import org.eclipse.graphiti.mm.algorithms.Polyline;
import org.eclipse.graphiti.services.IGaService;
import org.eclipselabs.spray.runtime.graphiti.features.AbstractAddConnectionFeature;
import org.eclipselabs.spray.runtime.graphiti.styles.ISprayStyle;
import org.modelversioning.emfprofile.graphiti.petrinet.styles.PetrinetDefaultStyle;
import org.eclipselabs.spray.runtime.graphiti.shape.ISprayConnection;
import org.modelversioning.emfprofile.graphiti.petrinet.shapes.ArrowConnectionShapeConnection;
import com.google.common.base.Function;
import org.modelversioning.emfprofile.graphiti.petrinet.Activator;
import petrinet.PetrinetPackage;

@SuppressWarnings("unused")
public abstract class MypetrinetAddInputArcFeatureBase extends AbstractAddConnectionFeature {

    public MypetrinetAddInputArcFeatureBase(final IFeatureProvider fp) {
        super(fp);
        gaService = Activator.get(IGaService.class);
    }

    /**
     * {@inheritDoc}
     * 
     * @return <code>true</code> if given business object is an {@link petrinet.InputArc} and context is of type {@link IAddConnectionContext}
     */
    @Override
    public boolean canAdd(IAddContext context) {
        if (context instanceof IAddConnectionContext && context.getNewObject() instanceof petrinet.InputArc) {
            return true;
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PictogramElement add(IAddContext context) {
        IAddConnectionContext addConContext = (IAddConnectionContext) context;
        // TODO: Domain object
        petrinet.InputArc addedDomainObject = (petrinet.InputArc) context.getNewObject();
        ISprayStyle style = new PetrinetDefaultStyle();
        ISprayConnection connection = new ArrowConnectionShapeConnection(getFeatureProvider());
        Connection result = (Connection) connection.getConnection(getDiagram(), style, addConContext.getSourceAnchor(), addConContext.getTargetAnchor());

        // create link and wire it
        peService.setPropertyValue(result, PROPERTY_MODEL_TYPE, PetrinetPackage.Literals.INPUT_ARC.getName());
        link(result, addedDomainObject);
        for (ConnectionDecorator conDecorator : result.getConnectionDecorators()) {
            link(conDecorator, addedDomainObject);
        }

        setDoneChanges(true);
        updatePictogramElement(result);

        return result;
    }

}
