/*************************************************************************************
 *
 * Generated on Thu Sep 04 16:12:39 CEST 2014 by Spray PasteFeature.xtend
 *
 * This file contains generated and should not be changed.
 * Use the extension point class (the direct subclass of this class) to add manual code
 *
 *************************************************************************************/
package org.modelversioning.emfprofile.graphiti.petrinet.features;

import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IPasteContext;
import org.eclipse.graphiti.features.context.impl.AddContext;
import org.eclipse.graphiti.mm.Property;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipselabs.spray.runtime.graphiti.features.AbstractPasteFeature;
import org.modelversioning.emfprofile.graphiti.petrinet.diagram.MypetrinetModelService;
import petrinet.PetrinetPackage;

public abstract class MypetrinetPasteFeatureBase extends AbstractPasteFeature {

    protected MypetrinetModelService modelService;

    public MypetrinetPasteFeatureBase(IFeatureProvider fp) {
        super(fp);
        modelService = MypetrinetModelService.getModelService(fp.getDiagramTypeProvider());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canPaste(IPasteContext context) {
        // TODO: only support pasting directly in the diagram
        PictogramElement[] pes = context.getPictogramElements();
        if (pes.length != 1 || !(pes[0] instanceof Diagram)) {
            return false;
        }
        // can paste, if all objects on the clipboard are PictogramElements with link on subclasses of petrinet.PetriNet
        Object[] fromClipboard = getFromClipboard();
        if (fromClipboard == null || fromClipboard.length == 0) {
            return false;
        }
        for (Object object : fromClipboard) {
            if (!(object instanceof PictogramElement)) {
                return false;
            } else if (!(getBusinessObjectForPictogramElement((PictogramElement) object) instanceof petrinet.PetriNet)) {
                return false;
            }
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void paste(IPasteContext context) {
        // already verified, that pasting is allowed just directly in the diagram
        PictogramElement[] pes = context.getPictogramElements();
        Diagram diagram = (Diagram) pes[0];

        // get the PictogramElements from the clipboard and the linked business object.
        Object[] objects = getFromClipboard();
        for (Object object : objects) {
            PictogramElement pictogramElement = (PictogramElement) object;
            petrinet.PetriNet boRef = (petrinet.PetriNet) getBusinessObjectForPictogramElement(pictogramElement);
            petrinet.PetriNet bo = EcoreUtil.copy(boRef);
            addBusinessObjectToContainer(bo, pictogramElement);

            // create a new AddContext for the creation of a new shape.
            AddContext ac = new AddContext(new AddContext(), bo);
            ac.setLocation(0, 0); // for simplicity paste at (0, 0)
            ac.setTargetContainer(diagram); // paste on diagram
            // copy all properties from the shape (e.g. ALIAS etc.)
            for (Property prop : pictogramElement.getProperties()) {
                ac.putProperty(prop.getKey(), prop.getValue());
            }
            getFeatureProvider().addIfPossible(ac);
        }
    }

    protected void addBusinessObjectToContainer(petrinet.PetriNet bo, PictogramElement pe) {
        final petrinet.PetriNet model = modelService.getModel();
        final String alias = Graphiti.getPeService().getPropertyValue(pe, PROPERTY_ALIAS);
        if (bo.eClass() == PetrinetPackage.Literals.PLACE && alias == null) {
            model.getElements().add((petrinet.Place) bo);
        }
        if (bo.eClass() == PetrinetPackage.Literals.TRANSITION && alias == null) {
            model.getElements().add((petrinet.Transition) bo);
        }
        if (bo.eClass() == PetrinetPackage.Literals.OUTPUT_ARC && alias == null) {
            model.getElements().add((petrinet.OutputArc) bo);
        }
        if (bo.eClass() == PetrinetPackage.Literals.INPUT_ARC && alias == null) {
            model.getElements().add((petrinet.InputArc) bo);
        }
    }
}
