/*************************************************************************************
 *
 * Generated on Thu Sep 04 16:12:39 CEST 2014 by Spray ResizeFeature.xtend
 *
 * This file contains generated and should not be changed.
 * Use the extension point class (the direct subclass of this class) to add manual code
 *
 *************************************************************************************/
package org.modelversioning.emfprofile.graphiti.petrinet.features;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.IResizeConfiguration;
import org.eclipse.graphiti.features.context.IResizeShapeContext;
import org.eclipse.graphiti.features.DefaultResizeConfiguration;
import org.eclipselabs.spray.runtime.graphiti.features.DefaultResizeShapeFeature;
import org.eclipselabs.spray.runtime.graphiti.shape.SprayLayoutManager;
import org.modelversioning.emfprofile.graphiti.petrinet.shapes.TransitionShapeShape;

public abstract class MypetrinetResizeTransitionFeatureBase extends DefaultResizeShapeFeature {

    SprayLayoutManager layoutManager;

    public MypetrinetResizeTransitionFeatureBase(final IFeatureProvider fp) {
        super(fp);
        layoutManager = new TransitionShapeShape(fp).getShapeLayout();
    }

    public class ResizeConfiguration_TransitionShape extends DefaultResizeConfiguration {

        public boolean isHorizontalResizeAllowed() {
            return layoutManager.isStretchHorizontal();
        }

        public boolean isVerticalResizeAllowed() {
            return layoutManager.isStretchVertical();
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IResizeConfiguration getResizeConfiguration(IResizeShapeContext context) {
        return new ResizeConfiguration_TransitionShape();
    }
}
