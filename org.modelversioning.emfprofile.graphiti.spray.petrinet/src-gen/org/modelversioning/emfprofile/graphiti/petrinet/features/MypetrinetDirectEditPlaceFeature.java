/*************************************************************************************
 *
 * Generated on Thu Sep 04 16:12:39 CEST 2014 by Spray DirectEditFeature.xtend
 * 
 * This file is an extension point: copy to "src" folder to manually add code to this
 * extension point.
 *
 *************************************************************************************/
package org.modelversioning.emfprofile.graphiti.petrinet.features;

import org.eclipse.graphiti.features.IFeatureProvider;

public class MypetrinetDirectEditPlaceFeature extends MypetrinetDirectEditPlaceFeatureBase {
    public MypetrinetDirectEditPlaceFeature(IFeatureProvider fp) {
        super(fp);
    }
}
