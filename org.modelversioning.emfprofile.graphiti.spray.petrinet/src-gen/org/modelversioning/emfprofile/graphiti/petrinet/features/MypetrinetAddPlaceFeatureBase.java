/*************************************************************************************
 *
 * Generated on Thu Sep 04 16:12:39 CEST 2014 by Spray AddShapeFromDslFeature.xtend
 *
 * This file contains generated and should not be changed.
 * Use the extension point class (the direct subclass of this class) to add manual code
 *
 *************************************************************************************/
package org.modelversioning.emfprofile.graphiti.petrinet.features;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IAddContext;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.Shape;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.services.IGaService;
import org.eclipselabs.spray.runtime.graphiti.features.AbstractAddFeature;
import org.eclipselabs.spray.runtime.graphiti.shape.ISprayShape;
import org.eclipselabs.spray.runtime.graphiti.layout.SprayLayoutService;
import org.eclipselabs.spray.runtime.graphiti.GraphitiProperties;
import org.modelversioning.emfprofile.graphiti.petrinet.shapes.PlaceShapeShape;
import org.modelversioning.emfprofile.graphiti.petrinet.styles.PetrinetDefaultStyle;
import org.eclipselabs.spray.runtime.graphiti.styles.ISprayStyle;

@SuppressWarnings("unused")
public abstract class MypetrinetAddPlaceFeatureBase extends AbstractAddFeature {
    protected final static String typeOrAliasName = "Place";
    protected Diagram             targetDiagram   = null;

    public MypetrinetAddPlaceFeatureBase(final IFeatureProvider fp) {
        super(fp);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canAdd(final IAddContext context) {
        final EObject newObject = (EObject) context.getNewObject();
        if (newObject instanceof petrinet.Place) {
            // check if user wants to add to a diagram
            if (context.getTargetContainer() instanceof Diagram) {
                return true;
            } else if (context.getTargetContainer() instanceof ContainerShape) {
                // OLD STUFF
                final Object target = getBusinessObjectForPictogramElement(context.getTargetContainer());
                // NEW stuff
            }
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PictogramElement add(final IAddContext context) {
        final petrinet.Place addedModelElement = (petrinet.Place) context.getNewObject();
        // NEW stuff
        Object target = getBusinessObjectForPictogramElement(context.getTargetContainer());
        final ContainerShape targetContainer = context.getTargetContainer();
        final ISprayStyle style = new PetrinetDefaultStyle();
        final ISprayShape shape = new PlaceShapeShape(getFeatureProvider());
        final ContainerShape conShape = shape.getShape(targetContainer, style);
        final IGaService gaService = Graphiti.getGaService();
        gaService.setLocation(conShape.getGraphicsAlgorithm(), context.getX(), context.getY());
        link(conShape, addedModelElement);
        linkShapes(conShape, addedModelElement);

        setDoneChanges(true);
        updatePictogramElement(conShape);
        layout(conShape);

        return conShape;
    }

    protected void linkShapes(ContainerShape conShape, petrinet.Place addedModelElement) {
        link(conShape, addedModelElement);
        for (Shape childShape : conShape.getChildren()) {
            if (childShape instanceof ContainerShape) {
                linkShapes((ContainerShape) childShape, addedModelElement);
            } else {
                link(childShape, addedModelElement);
            }
        }
    }
}
