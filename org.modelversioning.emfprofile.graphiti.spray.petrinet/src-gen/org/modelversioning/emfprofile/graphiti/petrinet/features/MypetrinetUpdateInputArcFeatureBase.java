/*************************************************************************************
 *
 * Generated on Thu Sep 04 16:12:39 CEST 2014 by Spray UpdateConnectionFromDslFeature.xtend
 *
 * This file contains generated and should not be changed.
 * Use the extension point class (the direct subclass of this class) to add manual code
 *
 *************************************************************************************/
package org.modelversioning.emfprofile.graphiti.petrinet.features;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import com.google.common.base.Function;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.IReason;
import org.eclipse.graphiti.features.context.IUpdateContext;
import org.eclipse.graphiti.features.impl.Reason;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.pictograms.Connection;
import org.eclipse.graphiti.mm.pictograms.ConnectionDecorator;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.services.IGaService;
import org.eclipselabs.spray.runtime.graphiti.features.AbstractUpdateFeature;
import org.modelversioning.emfprofile.graphiti.petrinet.Activator;

public abstract class MypetrinetUpdateInputArcFeatureBase extends AbstractUpdateFeature {

    public MypetrinetUpdateInputArcFeatureBase(final IFeatureProvider fp) {
        super(fp);
        gaService = Activator.get(IGaService.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canUpdate(final IUpdateContext context) {
        // return true, if linked business object is a EClass
        final PictogramElement pictogramElement = context.getPictogramElement();
        final EObject bo = getBusinessObjectForPictogramElement(pictogramElement);
        return (bo instanceof petrinet.InputArc) && (!(pictogramElement instanceof Diagram));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IReason updateNeeded(final IUpdateContext context) {
        final PictogramElement pictogramElement = context.getPictogramElement();
        final EObject bo = getBusinessObjectForPictogramElement(pictogramElement);
        if (!(bo instanceof petrinet.InputArc)) {
            return Reason.createFalseReason();
        }
        petrinet.InputArc eClass = (petrinet.InputArc) bo;
        if (pictogramElement instanceof Connection) {
            final Connection conShape = (Connection) pictogramElement;
            List<String> changedTypes = new ArrayList<String>();
            for (ConnectionDecorator dec : conShape.getConnectionDecorators()) {
                final GraphicsAlgorithm gAlg = dec.getGraphicsAlgorithm();
                searchChilds(gAlg, eClass, changedTypes, false);
                if (!changedTypes.isEmpty()) {
                    return Reason.createTrueReason(changedTypes.toString());
                }
            }
        }
        return Reason.createFalseReason();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean update(IUpdateContext context) {
        final PictogramElement pictogramElement = context.getPictogramElement();
        final petrinet.InputArc eClass = (petrinet.InputArc) getBusinessObjectForPictogramElement(pictogramElement);
        if (pictogramElement instanceof Connection) {
            final Connection conShape = (Connection) pictogramElement;
            for (ConnectionDecorator dec : conShape.getConnectionDecorators()) {
                final GraphicsAlgorithm gAlg = dec.getGraphicsAlgorithm();
                searchChilds(gAlg, eClass, new ArrayList<String>(), true);
            }
        }
        return true;
    }

    protected void searchChilds(GraphicsAlgorithm gAlg, petrinet.InputArc eClass, List<String> changedTypes, boolean performUpdate) {
        if (gAlg instanceof Text) {
            Text text = (Text) gAlg;
            String id = peService.getPropertyValue(gAlg, TEXT_ID);
            if (id.equals("arcName")) {
                if (performUpdate) {
                    text.setValue(eClass.getName());
                    setDoneChanges(true);
                } else {
                    String eClassValue = eClass.getName();
                    String gAlgorithmValue = text.getValue();
                    if (eClassValue != null) {
                        if (!eClassValue.equals(gAlgorithmValue)) {
                            changedTypes.add("arcName");
                        }
                    }
                }
            }
        } else {
            for (GraphicsAlgorithm gAlgChild : gAlg.getGraphicsAlgorithmChildren()) {
                searchChilds(gAlgChild, eClass, changedTypes, performUpdate);
            }
        }
    }
}
