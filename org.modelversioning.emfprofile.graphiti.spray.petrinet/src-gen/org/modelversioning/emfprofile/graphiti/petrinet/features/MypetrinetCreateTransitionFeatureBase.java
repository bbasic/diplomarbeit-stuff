/*************************************************************************************
 *
 * Generated on Thu Sep 04 16:12:39 CEST 2014 by Spray CreateShapeFeature.xtend
 *
 * This file contains generated and should not be changed.
 * Use the extension point class (the direct subclass of this class) to add manual code
 *
 *************************************************************************************/
package org.modelversioning.emfprofile.graphiti.petrinet.features;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.ICreateContext;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipselabs.spray.runtime.graphiti.containers.SampleUtil;
import org.eclipselabs.spray.runtime.graphiti.features.AbstractCreateFeature;
import org.eclipselabs.spray.runtime.graphiti.layout.SprayLayoutService;
import org.eclipselabs.spray.runtime.graphiti.GraphitiProperties;
import org.eclipse.graphiti.features.context.IAreaContext;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.features.context.impl.AddContext;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.modelversioning.emfprofile.graphiti.petrinet.diagram.MypetrinetModelService;
import petrinet.PetrinetFactory;

public abstract class MypetrinetCreateTransitionFeatureBase extends AbstractCreateFeature {
    protected static String          TITLE         = "Create ";
    protected static String          USER_QUESTION = "Enter new  name";
    protected MypetrinetModelService modelService;
    protected petrinet.Transition    newClass      = null;

    public MypetrinetCreateTransitionFeatureBase(final IFeatureProvider fp) {
        // set name and description of the creation feature
        super(fp, "Transition", "Create new Transition");
        modelService = MypetrinetModelService.getModelService(fp.getDiagramTypeProvider());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canCreate(final ICreateContext context) {
        final Object target = getBusinessObjectForPictogramElement(context.getTargetContainer());
        // TODO: Respect the cardinality of the containment reference
        if (context.getTargetContainer() instanceof Diagram) {
            return true;
        } else if (context.getTargetContainer() instanceof ContainerShape) {
        }
        // And now the new stuff
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object[] create(final ICreateContext context) {
        newClass = createTransition(context);

        if (newClass == null) {
            return EMPTY;
        }

        // do the add
        addGraphicalRepresentation(context, newClass);

        // activate direct editing after object creation
        getFeatureProvider().getDirectEditingInfo().setActive(true);

        // return newly created business object(s)
        return new Object[]{newClass};
    }

    //       org.eclipse.emf.ecore.impl.EReferenceImpl@5b2c9249 (name: elements) (ordered: true, unique: true, lowerBound: 0, upperBound: -1) (changeable: true, volatile: false, transient: false, defaultValueLiteral: null, unsettable: false, derived: false) (containment: true, resolveProxies: true) 

    /**
     * Creates a new {@link petrinet.Transition} instance and adds it to the containing type.
     */
    protected petrinet.Transition createTransition(final ICreateContext context) {
        // create Transition instance
        final petrinet.Transition newClass = PetrinetFactory.eINSTANCE.createTransition();
        // ask user for Transition name
        String newName = SampleUtil.askString(TITLE, USER_QUESTION, "", null);
        if (newName == null || newName.trim().length() == 0) {
            return null;
        } else {
            newClass.setName(newName);
        }
        ContainerShape targetContainer = context.getTargetContainer();
        boolean isContainment = false;
        final Object target = getBusinessObjectForPictogramElement(context.getTargetContainer());
        //              And now the NEW stuff
        if (!isContainment) {
            // add the element to containment reference
            petrinet.PetriNet model = modelService.getModel();
            model.getElements().add(newClass);
        }
        setDoneChanges(true);
        return newClass;
    }
}
