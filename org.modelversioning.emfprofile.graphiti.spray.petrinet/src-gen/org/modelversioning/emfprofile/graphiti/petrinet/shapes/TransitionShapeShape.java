/*************************************************************************************
 *
 * Generated on Thu Sep 04 16:28:04 CEST 2014 by Spray ShapeDefinitionGenerator.xtend
 * 
 * This file is an extension point: copy to "src" folder to manually add code to this
 * extension point.
 *
 *************************************************************************************/
package org.modelversioning.emfprofile.graphiti.petrinet.shapes;

import org.eclipse.graphiti.features.IFeatureProvider;

public class TransitionShapeShape extends TransitionShapeShapeBase {

    public TransitionShapeShape(IFeatureProvider fp) {
        super(fp);
    }
}
