/*************************************************************************************
 *
 * Generated on Thu Sep 04 16:28:04 CEST 2014 by Spray ConnectionDefinitionGenerator.xtend
 * 
 * This file is an extension point: copy to "src" folder to manually add code to this
 * extension point.
 *
 *************************************************************************************/
package org.modelversioning.emfprofile.graphiti.petrinet.shapes;

import org.eclipse.graphiti.features.IFeatureProvider;

public class ArrowConnectionShapeConnection extends ArrowConnectionShapeConnectionBase {

    public ArrowConnectionShapeConnection(IFeatureProvider fp) {
        super(fp);
    }
}
