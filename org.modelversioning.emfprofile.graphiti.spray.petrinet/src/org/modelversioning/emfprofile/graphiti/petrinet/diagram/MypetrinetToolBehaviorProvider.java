/*************************************************************************************
 *
 * Generated on Fri Dec 06 12:38:43 CET 2013 by Spray ToolBehaviorProvider.xtend
 * 
 * This file is an extension point: copy to "src" folder to manually add code to this
 * extension point.
 *
 *************************************************************************************/
package org.modelversioning.emfprofile.graphiti.petrinet.diagram;

import java.util.List;

import org.eclipse.graphiti.dt.IDiagramTypeProvider;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.tb.IDecorator;
import org.eclipse.graphiti.ui.editor.DiagramBehavior;
import org.eclipselabs.spray.runtime.graphiti.tb.IRenderingDecoratorProvider;
import org.modelversioning.emfprofile.application.decorator.graphiti.provider.DecoratorsProvider;

import petrinet.Place;

import com.google.common.collect.Lists;

public class MypetrinetToolBehaviorProvider extends MypetrinetToolBehaviorProviderBase {
	
	List<IRenderingDecoratorProvider> renderingProviders = Lists.newArrayList();

	IRenderingDecoratorProvider testRenderingDecoratorProvider = new IRenderingDecoratorProvider() {
		
		@Override
		public IDecorator[] getDecorators(PictogramElement pe) {
//			if(getFeatureProvider().getBusinessObjectForPictogramElement(pe) instanceof Place){
//				System.out.println("PLACE pictogram element: " + pe);
//				System.out.println("PictogramElement Contents: " + pe.eContents().size());
//				for (EObject content : pe.eContents()) {
//					System.out.println("\t" + content.toString());
//				}
//				GraphicsAlgorithm ga = pe.getGraphicsAlgorithm();
//				System.out.println("Graphics algorith: " + ga);
//				System.out.println("Graphics algorith Contents: " + ga.eContents().size());
//				for (EObject content : ga.eContents()) {
//					System.out.println("\t" + content.toString());
//				}
				
//				BorderDecorator bd = new BorderDecorator();
//				bd.setBorderColor(IColorConstant.RED);
//				bd.setBorderWidth(1);
//				
//				ColorDecorator cd = new ColorDecorator();
//				cd.setForegroundColor(IColorConstant.DARK_GREEN);
//				cd.setBackgroundColor(IColorConstant.BLUE);
//				cd.setMessage("this is the message of the color decorator.");
//				return new IDecorator[]{bd, cd};			
//				return DecoratorsProvider.INSTANCE.getDecorators(pe, getDiagramTypeProvider().getDiagramBehavior());
//			}
//			pe.getGraphicsAlgorithm().
//			getDiagramTypeProvider().getDiagramBehavior().refreshRenderingDecorators(pe);
			
//			here we could call the graphiti decorator and give him the pictogram element and the diagram behavior objects
			
			// TODO Auto-generated method stub
			return DecoratorsProvider.INSTANCE.getDecorators(pe, getDiagramTypeProvider().getDiagramBehavior());
		}
	};
	
	public MypetrinetToolBehaviorProvider(final IDiagramTypeProvider dtp) {
        super(dtp);
        ((DiagramBehavior)dtp.getDiagramBehavior()).getUpdateBehavior().setAdapterActive(false);;
    }
    
    
    @Override
    protected IRenderingDecoratorProvider[] getRenderingDecoratorProviders() {
    	// TODO Auto-generated method stub
//    	return super.getRenderingDecoratorProviders();
    	return new IRenderingDecoratorProvider[] {testRenderingDecoratorProvider};
    }
    
}
