/*************************************************************************************
 *
 * Generated on Fri Dec 06 16:42:22 CET 2013 by Spray FeatureProvider.xtend
 * 
 * This file is an extension point: copy to "src" folder to manually add code to this
 * extension point.
 *
 *************************************************************************************/
package org.modelversioning.emfprofile.graphiti.petrinet.diagram;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.dt.IDiagramTypeProvider;
import org.eclipse.graphiti.features.IAddFeature;
import org.eclipse.graphiti.features.context.IAddContext;

public class MypetrinetFeatureProvider extends MypetrinetFeatureProviderBase {

    public MypetrinetFeatureProvider(final IDiagramTypeProvider dtp) {
        super(dtp);
    }
    
    @Override
    public IAddFeature getAddFeature(IAddContext context) {
    	if(context.getNewObject() instanceof EObject)
    		return super.getAddFeature(context);
    	return null;
    }
}
