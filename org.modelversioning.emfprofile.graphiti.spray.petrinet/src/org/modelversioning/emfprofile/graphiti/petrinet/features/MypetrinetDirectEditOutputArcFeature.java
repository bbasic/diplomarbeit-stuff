/*************************************************************************************
 *
 * Generated on Fri Dec 06 15:59:42 CET 2013 by Spray DirectEditFeature.xtend
 * 
 * This file is an extension point: copy to "src" folder to manually add code to this
 * extension point.
 *
 *************************************************************************************/
package org.modelversioning.emfprofile.graphiti.petrinet.features;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IDirectEditingContext;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;

import com.google.common.base.Function;

public class MypetrinetDirectEditOutputArcFeature extends MypetrinetDirectEditOutputArcFeatureBase {
    public MypetrinetDirectEditOutputArcFeature(IFeatureProvider fp) {
        super(fp);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public String getInitialValue(IDirectEditingContext context) {
        // return the initial value of the clicked text on the petrinet.OutputArc
        PictogramElement pe = context.getPictogramElement();
        petrinet.OutputArc eClass = (petrinet.OutputArc) getBusinessObjectForPictogramElement(pe);
        Text gAlg = (Text) context.getGraphicsAlgorithm();
        String id = peService.getPropertyValue(gAlg, TEXT_ID);
        {
            if (id.equals("arcName")) {
                return eClass.getName();
            }
        }
        {
            String gAlgValue = new Function<petrinet.OutputArc, String>() {
                public String apply(petrinet.OutputArc modelElement) {
                    return modelElement.getFrom().getName();/* could not compile <XFeatureCallImplCustom>.name with type JvmParameterizedTypeReference: java.lang.String because of null */
                }
            }.apply(eClass);
            if (id.equals("sourceText")) {
                return gAlgValue;
            }
        }
        {
            String gAlgValue = new Function<petrinet.OutputArc, String>() {
                public String apply(petrinet.OutputArc modelElement) {
                	return modelElement.getTo().getName();/* could not compile <XFeatureCallImplCustom>.name with type JvmParameterizedTypeReference: java.lang.String because of null */
                }
            }.apply(eClass);
            if (id.equals("targetText")) {
                return gAlgValue;
            }
        }
        return "";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getEditingType() {
        return TYPE_TEXT;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setValue(final String value, final IDirectEditingContext context) {
        // set the new value on the petrinet.OutputArc
        final PictogramElement pe = context.getPictogramElement();
        petrinet.OutputArc eClass = (petrinet.OutputArc) getBusinessObjectForPictogramElement(pe);
        Text gAlg = (Text) context.getGraphicsAlgorithm();
        String id = peService.getPropertyValue(gAlg, TEXT_ID);
        {
            if (id.equals("arcName")) {
                eClass.setName(value);
            }
        }
        {
            String gAlgValue = new Function<petrinet.OutputArc, String>() {
                public String apply(petrinet.OutputArc modelElement) {
                    return modelElement.getFrom().getName();/* could not compile <XFeatureCallImplCustom>.name with type JvmParameterizedTypeReference: java.lang.String because of null */
                }
            }.apply(eClass);
            if (id.equals("sourceText")) {
                gAlg.setValue(gAlgValue);
            }
        }
        {
            String gAlgValue = new Function<petrinet.OutputArc, String>() {
                public String apply(petrinet.OutputArc modelElement) {
                    return modelElement.getTo().getName();/* could not compile <XFeatureCallImplCustom>.name with type JvmParameterizedTypeReference: java.lang.String because of null */
                }
            }.apply(eClass);
            if (id.equals("targetText")) {
                gAlg.setValue(gAlgValue);
            }
        }
        updatePictogramElement(pe);
    }
}
