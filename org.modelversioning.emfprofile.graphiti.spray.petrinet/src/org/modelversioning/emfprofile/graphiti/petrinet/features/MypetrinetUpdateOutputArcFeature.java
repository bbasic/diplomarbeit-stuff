/*************************************************************************************
 *
 * Generated on Fri Dec 06 15:59:42 CET 2013 by Spray UpdateConnectionFromDslFeature.xtend
 * 
 * This file is an extension point: copy to "src" folder to manually add code to this
 * extension point.
 *
 *************************************************************************************/
package org.modelversioning.emfprofile.graphiti.petrinet.features;

import java.util.List;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.algorithms.Text;

import com.google.common.base.Function;

public class MypetrinetUpdateOutputArcFeature extends MypetrinetUpdateOutputArcFeatureBase {
    public MypetrinetUpdateOutputArcFeature(final IFeatureProvider fp) {
        super(fp);
    }
    
    protected void searchChilds(GraphicsAlgorithm gAlg, petrinet.OutputArc eClass, List<String> changedTypes, boolean performUpdate) {
        if (gAlg instanceof Text) {
            Text text = (Text) gAlg;
            String id = peService.getPropertyValue(gAlg, TEXT_ID);
            if (id.equals("arcName")) {
                if (performUpdate) {
                    text.setValue(eClass.getName());
                    setDoneChanges(true);
                } else {
                    String eClassValue = eClass.getName();
                    String gAlgorithmValue = text.getValue();
                    if (eClassValue != null) {
                        if (!eClassValue.equals(gAlgorithmValue)) {
                            changedTypes.add("arcName");
                        }
                    }
                }
            }
            if (id.equals("sourceText")) {
                if (performUpdate) {
                    String value = new Function<petrinet.OutputArc, String>() {
                        public String apply(petrinet.OutputArc modelElement) {
                            return modelElement.getFrom().getName();/* could not compile <XFeatureCallImplCustom>.name with type JvmParameterizedTypeReference: java.lang.String because of null */
                        }
                    }.apply(eClass);
                    text.setValue(value);
                    setDoneChanges(true);
                } else {
                    String eClassValue = new Function<petrinet.OutputArc, String>() {
                        public String apply(petrinet.OutputArc modelElement) {
                        	return modelElement.getFrom().getName();/* could not compile <XFeatureCallImplCustom>.name with type JvmParameterizedTypeReference: java.lang.String because of null */
                        }
                    }.apply(eClass);
                    String gAlgorithmValue = text.getValue();
                    if (eClassValue != null) {
                        if (!eClassValue.equals(gAlgorithmValue)) {
                            changedTypes.add("sourceText");
                        }
                    }
                }
            }
            if (id.equals("targetText")) {
                if (performUpdate) {
                    String value = new Function<petrinet.OutputArc, String>() {
                        public String apply(petrinet.OutputArc modelElement) {
                        	return modelElement.getTo().getName();/* could not compile <XFeatureCallImplCustom>.name with type JvmParameterizedTypeReference: java.lang.String because of null */
                        }
                    }.apply(eClass);
                    text.setValue(value);
                    setDoneChanges(true);
                } else {
                    String eClassValue = new Function<petrinet.OutputArc, String>() {
                        public String apply(petrinet.OutputArc modelElement) {
                        	return modelElement.getTo().getName();/* could not compile <XFeatureCallImplCustom>.name with type JvmParameterizedTypeReference: java.lang.String because of null */
                        }
                    }.apply(eClass);
                    String gAlgorithmValue = text.getValue();
                    if (eClassValue != null) {
                        if (!eClassValue.equals(gAlgorithmValue)) {
                            changedTypes.add("targetText");
                        }
                    }
                }
            }
        } else {
            for (GraphicsAlgorithm gAlgChild : gAlg.getGraphicsAlgorithmChildren()) {
                searchChilds(gAlgChild, eClass, changedTypes, performUpdate);
            }
        }
    }
}
