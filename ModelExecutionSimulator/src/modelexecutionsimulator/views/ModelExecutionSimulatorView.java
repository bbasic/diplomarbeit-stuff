package modelexecutionsimulator.views;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.edit.domain.IEditingDomainProvider;
import org.eclipse.gmf.runtime.diagram.ui.parts.DiagramEditor;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.FontDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;
import org.modelversioning.emfprofile.Profile;
import org.modelversioning.emfprofile.Stereotype;
import org.modelversioning.emfprofile.application.registry.ProfileApplicationManager;
import org.modelversioning.emfprofile.application.registry.ProfileApplicationRegistry;
import org.modelversioning.emfprofile.application.registry.ProfileApplicationWrapper;
import org.modelversioning.emfprofile.application.registry.exception.ProfileApplicationDecoratorNotFoundException;
import org.modelversioning.emfprofile.application.registry.exception.ReadingDecorationDescriptionsException;
import org.modelversioning.emfprofileapplication.StereotypeApplication;

import petrinet.Arc;
import petrinet.InputArc;
import petrinet.OutputArc;
import petrinet.PetriNet;
import petrinet.Place;
import petrinet.Transition;

import com.google.common.collect.Lists;

public class ModelExecutionSimulatorView extends ViewPart {

	private static final String INACTIVE_TEXT = "Inactive!";
	/**
	 * The ID of the view as specified by the extension.
	 */
	public static final String ID = "modelexecutionsimulator.views.ModelExecutionSimulatorView";
	private Label stepsCountDisplay;
	private Button startStopSimulation;
	private Button nextStep;
	private boolean simulationActive = false;
	private int stepCounter = 0;
	private int maxSteps = 14;

	IEditorPart editor = null;
	ResourceSet resourceSet = null;
	private ProfileApplicationManager profileApplicationManager;

	IFile profileFile = ResourcesPlugin
			.getWorkspace()
			.getRoot()
			.getFile(
					new Path(
							"ForkJoin_PetriNet_ExecutionProfile/profile.emfprofile_diagram"));
	private EObject petriNet;
	private Resource profileResource;
	private IFile profileApplicationFile;
	private ProfileApplicationWrapper profileApplicationWrapper;
	private Profile profile;
	private Stereotype tokonStereotype;
	private Stereotype activationStereotype;

	// ////////////// Places names /////////////////
	String mainThreadStartName = "Main Thread Start";
	String mainThreadName = "Main Thread";
	String mainThreadEndName = "Main Thread End";

	String threadAStartName = "Thread A Start";
	String threadACritcalSectionName = "Thread A critical section";
	String threadAEndName = "Thread A End";

	String threadBStartName = "Thread B Start";
	String threadBCritcalSectionName = "Thread B critical section";
	String threadBEndName = "Thread B End";

	String semaphoreName = "Semaphore";

	// /////////// Transitions names //////////////
	String forkName = "FORK";
	String joinName = "JOIN";
	String lockInThreadAName = "LOCK in Thread A";
	String lockInThreadBName = "LOCK in Thread B";
	String unlockInThreadAName = "UNLOCK in Thread A";
	String unlockInThreadBName = "UNLOCK in Thread B";

	// ////////////////////////////////////////////////

	Place placeMainThreadStart = null;
	Place placeMainThread = null;
	Place placeMainThreadEnd = null;
	Place placeThreadAStart = null;
	Place placeThreadACriticalSection = null;
	Place placeThreadAEnd = null;
	Place placeThreadBStart = null;
	Place placeThreadBCriticalSection = null;
	Place placeThreadBEnd = null;
	Place placeSemaphore = null;

	Transition transFork = null;
	Transition transJoin = null;
	Transition transLockInThreadA = null;
	Transition transLockInThreadB = null;
	Transition transUnlockInThreadA = null;
	Transition transUnlockInThreadB = null;
	private Font boldFont;
	

	/**
	 * The constructor.
	 */
	public ModelExecutionSimulatorView() {
	}

	/**
	 * This is a callback that will allow us to create the viewer and initialize
	 * it.
	 */
	public void createPartControl(Composite parent) {
		parent.setLayout(new GridLayout(1, false));
		stepsCountDisplay = new Label(parent, SWT.CENTER);
		stepsCountDisplay.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		stepsCountDisplay.setText(INACTIVE_TEXT);
		FontDescriptor boldDescriptor = FontDescriptor.createFrom(stepsCountDisplay.getFont()).setStyle(SWT.BOLD).setHeight(15);
		boldFont = boldDescriptor.createFont(stepsCountDisplay.getDisplay());
		stepsCountDisplay.setFont(boldFont);
		
		startStopSimulation = new Button(parent, SWT.PUSH);
		startStopSimulation.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		startStopSimulation.setText("Start/Stop Simulation");
		nextStep = new Button(parent, SWT.PUSH);
		nextStep.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		nextStep.setText("Next Step");
		nextStep.setEnabled(false);

		startStopSimulation.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (simulationActive) {
					stopSimulation();

				} else {
					startSimulation();
				}
			}

		});

		nextStep.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				processNextStep();
			}
		});
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	public void setFocus() {
		startStopSimulation.setFocus();
	}

	protected void startSimulation() {
		Boolean isGMF = null;
		
		// check if we can access the right editor for simulation
		IEditorPart tempEditor = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage().getActiveEditor();
		if (tempEditor == null) {
			System.out.println("Active Editor is null");
			MessageDialog
					.openError(
							PlatformUI.getWorkbench()
									.getActiveWorkbenchWindow().getShell(),
							"Editor is not open!",
							"Editor with diagramm for execution simulation is not open. You have to open it first.");
			return;
		}
		if (tempEditor instanceof DiagramEditor) {
			isGMF = Boolean.TRUE;
			editor = tempEditor;
		} else if(tempEditor instanceof org.eclipse.graphiti.ui.editor.DiagramEditor) {
			isGMF = Boolean.FALSE;
			editor = tempEditor;
		} else {
			System.out.println("Editor is not a DiagramEditor");
			MessageDialog
			.openError(
					PlatformUI.getWorkbench()
							.getActiveWorkbenchWindow().getShell(),
					"Editor is not a Diagram Editor!",
					"This is not the Editor with diagramm for execution simulation. \nYou have to active it or open if it is not already opened.\nCurrently opened file in active editor is: " + tempEditor.getTitle());
			return;
		}
//		editor = (DiagramEditor) tempEditor;
//		System.out.println("Editor: " + editor);
		resourceSet = getResourceSet(editor);
		if (resourceSet == null) {
			System.out.println("ResourceSet is null");
			return;
		}

		try {
			profileApplicationManager = ProfileApplicationRegistry.INSTANCE
					.getProfileApplicationManager(resourceSet,
							getEditorIdFromEditorPart(editor));
			// PetrinetDiagramEditor
			// paManager.
		} catch (NullPointerException
				| ProfileApplicationDecoratorNotFoundException
				| ReadingDecorationDescriptionsException e) {
			e.printStackTrace();
			return;
		}
		System.out.println("ResourceSet: " + resourceSet);
		if(isGMF){
			petriNet = ((DiagramEditor)editor).getDiagram().getElement();
		} else {
			org.eclipse.graphiti.ui.editor.DiagramEditor _editor = (org.eclipse.graphiti.ui.editor.DiagramEditor)editor;
			Diagram diagram = _editor.getDiagramBehavior().getDiagramTypeProvider().getDiagram();
			System.out.println("Diagram: " + diagram);
			System.out.println("First Object in Contents: " + diagram.eResource().getContents().get(0));
			PictogramElement pe = (PictogramElement)diagram.eResource().getContents().get(0);
			System.out.println("business object: " + Graphiti.getLinkService().getBusinessObjectForLinkedPictogramElement(pe));
			petriNet = Graphiti.getLinkService().getBusinessObjectForLinkedPictogramElement(pe);
		}
		
		if (!(petriNet instanceof PetriNet)) {
			System.out
					.println("This is not the right diagram.\n Could not find the PetriNet element at the root of the diagram.\nFound element is: " + petriNet.toString());
			MessageDialog
					.openError(
							PlatformUI.getWorkbench()
									.getActiveWorkbenchWindow().getShell(),
							"This is not the right diagram!",
							"Could not find the PetriNet element at the root of the diagram. This is not the right diagram for execution simulation!");
			return;
		}
		System.out.println("PetriNet EObject: " + petriNet);
		System.out.println("And all contained elements:");
		TreeIterator<EObject> ti = EcoreUtil.getAllContents(petriNet
				.eContents());
		while (ti.hasNext()) {
			System.out.println("\t" + ti.next());
		}

		// initialize places and transitions variables
		{
			placeMainThread = getPlace(mainThreadName);
			placeMainThreadEnd = getPlace(mainThreadEndName);
			placeMainThreadStart = getPlace(mainThreadStartName);
			placeSemaphore = getPlace(semaphoreName);
			placeThreadACriticalSection = getPlace(threadACritcalSectionName);
			placeThreadAEnd = getPlace(threadAEndName);
			placeThreadAStart = getPlace(threadAStartName);
			placeThreadBCriticalSection = getPlace(threadBCritcalSectionName);
			placeThreadBEnd = getPlace(threadBEndName);
			placeThreadBStart = getPlace(threadBStartName);

			transFork = getTransition(forkName);
			transJoin = getTransition(joinName);
			transLockInThreadA = getTransition(lockInThreadAName);
			transLockInThreadB = getTransition(lockInThreadBName);
			transUnlockInThreadA = getTransition(unlockInThreadAName);
			transUnlockInThreadB = getTransition(unlockInThreadBName);
		}
		System.out.println("Profile File: "
				+ profileFile.getFullPath().toString() + ", exists: "
				+ profileFile.exists());
		URI profileUri = URI.createPlatformResourceURI(profileFile
				.getFullPath().toString(), true);
		profileResource = resourceSet.getResource(profileUri, true);
		if (profileResource == null) {
			System.out.println("Profile Resource is null! For uri: "
					+ profileUri.toString());
			return;
		}
		profile = (Profile) profileResource.getContents().get(0);
		System.out.println("Profile: " + profile
				+ ", has following stereotypes:");
		for (Stereotype st : profile.getStereotypes()) {
			System.out.println(st);
		}
		IProject project = ResourcesPlugin.getWorkspace().getRoot()
				.getProject("ForkJoin_Petrinet_Example");
		profileApplicationFile = project
				.getFile("fork-join-execution-simulation.pa.xmi");
		if (profileApplicationFile.exists()) {
			try {
				profileApplicationFile.delete(true, new NullProgressMonitor());
			} catch (CoreException e) {
				e.printStackTrace();
			}
		}

		try {
			profileApplicationWrapper = profileApplicationManager
					.createNewProfileApplication(profileApplicationFile,
							Lists.newArrayList(profile));
		} catch (IllegalArgumentException | IOException | CoreException
				| ReadingDecorationDescriptionsException e) {
			e.printStackTrace();
			return;
		}

		tokonStereotype = checkNotNull(profile.getStereotype("Token"));
		activationStereotype = checkNotNull(profile.getStereotype("Activation"));
		System.out.println("Token Stereotype: " + tokonStereotype);
		System.out.println("Activation Stereotype: " + activationStereotype);

		nextStep.setEnabled(true);
		simulationActive = true;
		displayStepsCount();
	}

	protected void stopSimulation() {
		nextStep.setEnabled(false);
		simulationActive = false;
		profileApplicationManager.dispose();
		stepCounter = 0;
		Display.getCurrent().asyncExec(new Runnable() {
			@Override
			public void run() {
				stepsCountDisplay.setText(INACTIVE_TEXT);
			}
		});
	}

	private void displayStepsCount() {
		Display.getCurrent().asyncExec(new Runnable() {
			@Override
			public void run() {
				stepsCountDisplay.setText(stepCounter + " of " + maxSteps + " Steps");
			}
		});
	}

	protected void processNextStep() {
		stepCounter++;
		displayStepsCount();
		switch (stepCounter) {
		case 1:
			step1();
			step2();
			break;

		case 2:
			step3();
			break;

		case 3:
			step4();
			step5();
			step6();
			step7();
			break;

		case 4:
			step8();
			break;

		case 5:
			step9();
			step10();
			step11();
			step12();
			break;

		case 6:
			step13();
			break;
		case 7:
			step14();
			step15();
			step16();
			step17();
			break;

		case 8:
			step18();
			break;

		case 9:
			step19();
			step20();
			step21();
			step22();
			break;

		case 10:
			step23();
			break;

		case 11:
			step24();
			step25();
			step26();
			step27();
			break;

		case 12:
			step28();
			break;

		case 13:
			step29();
			step30();
			step31();
			break;
		case 14:
			step32();
			break;

		default:
			nextStep.setEnabled(false);
			MessageDialog.openInformation(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), "End of simulation!", "You came at the end of this fork-join petri net model execution simulation.");
			stepCounter--;
			displayStepsCount();
			break;
		}
	}

	private void step1() {
		applyTokenToPlaces(placeSemaphore, placeMainThreadStart);
		setTaggedValueForPlaces(1, placeSemaphore);
		setTaggedValueForPlaces(3, placeMainThreadStart);
		applyActivationToInputArcs(placeSemaphore, placeMainThreadStart);
	}

	private void step2() {
		applyActivationToTransitions(transFork);
		applyActivationToOutputArcs(placeMainThread, placeThreadAStart,
				placeThreadBStart);
	}

	private void step3() {
		// activate all activations and set token stereotypes to the main
		// thread, thread a start, thread b start
		setTaggedValueForInputArcs(true, placeMainThreadStart);
		setTaggedValueForTransitions(true, transFork);
		setTaggedValueForOutputArcs(true, placeMainThread, placeThreadAStart,
				placeThreadBStart);

	}

	private void step4() {
		applyTokenToPlaces(placeMainThread, placeThreadAStart,
				placeThreadBStart);
	}

	private void step5() {
		removeStereotypeApplicationFromEObjects(placeMainThreadStart, transFork);
		removeStereotypeApplicationFromInputArcsWithPlaces(placeMainThreadStart);
		removeStereotypeApplicationFromOutputArcsWithPlaces(placeMainThread,
				placeThreadAStart, placeThreadBStart);
	}

	private void step6() {
		setTaggedValueForPlaces(1, placeMainThread, placeThreadAStart,
				placeThreadBStart);
		applyActivationToInputArcs(placeMainThread, placeThreadAStart,
				placeThreadBStart);
	}

	private void step7() {
		applyActivationToTransitions(transLockInThreadA, transLockInThreadB);
		applyActivationToOutputArcs(placeThreadACriticalSection,
				placeThreadBCriticalSection);
	}

	private void step8() {
		setTaggedValueForInputArcs(true, placeThreadBStart);
		setTaggedValueForOutputArcs(true, placeThreadBCriticalSection);
		for (InputArc ia : getInputArcs(placeSemaphore)) {
			if (ia.getTo().equals(transLockInThreadB)) {
				setTaggedValueForArcs(true, ia);
			}
		}
		setTaggedValueForTransitions(true, transLockInThreadB);
	}

	private void step9() {
		applyTokenToPlaces(placeThreadBCriticalSection);
	}

	private void step10() {
		removeStereotypeApplicationFromEObjects(placeThreadBStart,
				placeSemaphore, transLockInThreadA, transLockInThreadB);
		removeStereotypeApplicationFromInputArcsWithPlaces(placeSemaphore,
				placeThreadBStart);
		removeStereotypeApplicationFromOutputArcsWithPlaces(
				placeThreadACriticalSection, placeThreadBCriticalSection);
	}

	private void step11() {
		setTaggedValueForPlaces(2, placeThreadBCriticalSection);
		applyActivationToInputArcs(placeThreadBCriticalSection);
	}

	private void step12() {
		applyActivationToTransitions(transUnlockInThreadB);
		applyActivationToOutputArcs(placeThreadBEnd);
		for (OutputArc oa : getOutputArcs(placeSemaphore)) {
			if (oa.getFrom().equals(transUnlockInThreadB)) {
				profileApplicationWrapper.applyStereotype(activationStereotype,
						oa);
			}
		}
	}

	private void step13() {
		setTaggedValueForInputArcs(true, placeThreadBCriticalSection);
		setTaggedValueForTransitions(true, transUnlockInThreadB);
		setTaggedValueForOutputArcs(true, placeThreadBEnd);
		for (OutputArc oa : getOutputArcs(placeSemaphore)) {
			if (oa.getFrom().equals(transUnlockInThreadB)) {
				setTaggedValueForArcs(true, oa);
			}
		}
	}

	private void step14() {
		applyTokenToPlaces(placeSemaphore, placeThreadBEnd);
		removeStereotypeApplicationFromEObjects(placeThreadBCriticalSection);
	}

	private void step15() {
		removeStereotypeApplicationFromEObjects(transUnlockInThreadB);
		removeStereotypeApplicationFromInputArcsWithPlaces(placeThreadBCriticalSection);
		removeStereotypeApplicationFromOutputArcsWithPlaces(placeSemaphore,
				placeThreadBEnd);
	}

	private void step16() {
		setTaggedValueForPlaces(1, placeThreadBEnd, placeSemaphore);
		applyActivationToInputArcs(placeSemaphore, placeThreadBEnd);
	}

	private void step17() {
		applyActivationToTransitions(transLockInThreadA);
		applyActivationToOutputArcs(placeThreadACriticalSection);
	}

	private void step18() {
		setTaggedValueForTransitions(true, transLockInThreadA);
		setTaggedValueForInputArcs(true, placeThreadAStart);
		setTaggedValueForOutputArcs(true, placeThreadACriticalSection);
		for (InputArc ia : getInputArcs(placeSemaphore)) {
			if (ia.getTo().equals(transLockInThreadA)) {
				setTaggedValueForArcs(true, ia);
			}
		}
	}

	private void step19() {
		applyTokenToPlaces(placeThreadACriticalSection);
		removeStereotypeApplicationFromEObjects(placeSemaphore,
				placeThreadAStart);
	}

	private void step20() {
		removeStereotypeApplicationFromInputArcsWithPlaces(placeSemaphore, placeThreadAStart);
		removeStereotypeApplicationFromOutputArcsWithPlaces(placeThreadACriticalSection);
		removeStereotypeApplicationFromEObjects(transLockInThreadA);
	}
	
	private void step21() {
		applyActivationToInputArcs(placeThreadACriticalSection);
		setTaggedValueForPlaces(2, placeThreadACriticalSection);
	}
	private void step22() {
		applyActivationToTransitions(transUnlockInThreadA);
		applyActivationToOutputArcs(placeThreadAEnd);
		for (OutputArc oa : getOutputArcs(placeSemaphore)) {
			if (oa.getFrom().equals(transUnlockInThreadA)) {
				profileApplicationWrapper.applyStereotype(activationStereotype,
						oa);
			}
		}
	}
	private void step23() {
		setTaggedValueForInputArcs(true, placeThreadACriticalSection);
		setTaggedValueForTransitions(true, transUnlockInThreadA);
		setTaggedValueForOutputArcs(true, placeThreadAEnd);
		for (OutputArc oa : getOutputArcs(placeSemaphore)) {
			if (oa.getFrom().equals(transUnlockInThreadA)) {
				setTaggedValueForArcs(true, oa);
			}
		}
	}
	private void step24() {
		applyTokenToPlaces(placeSemaphore, placeThreadAEnd);
		removeStereotypeApplicationFromEObjects(placeThreadACriticalSection);
	}
	private void step25() {
		removeStereotypeApplicationFromEObjects(transUnlockInThreadA);
		removeStereotypeApplicationFromInputArcsWithPlaces(placeThreadACriticalSection);
		removeStereotypeApplicationFromOutputArcsWithPlaces(placeSemaphore,
				placeThreadAEnd);
	}
	
	private void step26() {
		setTaggedValueForPlaces(1, placeThreadAEnd, placeSemaphore);
		applyActivationToInputArcs(placeSemaphore, placeThreadAEnd);
	}

	private void step27() {
		applyActivationToTransitions(transJoin);
		applyActivationToOutputArcs(placeMainThreadEnd);
	}

	private void step28() {
		setTaggedValueForTransitions(true, transJoin);
		setTaggedValueForInputArcs(true, placeMainThread, placeThreadAEnd, placeThreadBEnd);
		setTaggedValueForOutputArcs(true, placeMainThreadEnd);
	}

	private void step29() {
		setTaggedValueForPlaces(-1, placeMainThread, placeThreadAEnd, placeThreadBEnd);
		applyTokenToPlaces(placeMainThreadEnd);
	}
	
	private void step30() {
		removeStereotypeApplicationFromEObjects(placeMainThread, placeThreadAEnd, placeThreadBEnd, transJoin);
		removeStereotypeApplicationFromInputArcsWithPlaces(placeMainThread, placeThreadAEnd, placeThreadBEnd);
		removeStereotypeApplicationFromOutputArcsWithPlaces(placeMainThreadEnd);
	}

	private void step31() {
		setTaggedValueForPlaces(3, placeMainThreadEnd);
	}

	private void step32() {
		setTaggedValueForPlaces(0, placeMainThreadEnd);
		setTaggedValueForPlaces(0, placeSemaphore);
		removeStereotypeApplicationFromInputArcsWithPlaces(placeSemaphore);
//		removeStereotypeApplicationFromEObjects(placeSemaphore);
	}

	// /////// UTIL METHODS /////////

	private void applyTokenToPlaces(Place... places) {
		for (Place place : places) {
			profileApplicationWrapper.applyStereotype(tokonStereotype, place);
		}
	}

	private void applyActivationToTransitions(Transition... transitions) {
		for (Transition transition : transitions) {
			profileApplicationWrapper.applyStereotype(activationStereotype,
					transition);
		}
	}

	private void applyActivationToOutputArcs(Place... places) {
		for (Place place : places) {
			for (OutputArc oa : getOutputArcs(place)) {
				profileApplicationWrapper.applyStereotype(activationStereotype,
						oa);
			}
		}
	}

	private void applyActivationToInputArcs(Place... places) {
		for (Place place : places) {
			for (InputArc ia : getInputArcs(place)) {
				profileApplicationWrapper.applyStereotype(activationStereotype,
						ia);
			}
		}
	}

	private void setTaggedValueForPlaces(int amount, Place... places) {
		for (Place place : places) {
			StereotypeApplication sa = profileApplicationWrapper
					.getStereotypeApplications(place).get(0);
			profileApplicationWrapper.setTaggedValue(sa, sa.eClass()
					.getEStructuralFeature("amount"), amount);
		}
	}

	private void setTaggedValueForTransitions(Boolean activated,
			Transition... transitions) {
		for (Transition transition : transitions) {
			StereotypeApplication sa = profileApplicationWrapper
					.getStereotypeApplications(transition).get(0);
			profileApplicationWrapper.setTaggedValue(sa, sa.eClass()
					.getEStructuralFeature("activated"), activated);
		}
	}

	private void setTaggedValueForInputArcs(Boolean activated, Place... places) {
		for (Place place : places) {
			for (InputArc ia : getInputArcs(place)) {
				StereotypeApplication sa = profileApplicationWrapper
						.getStereotypeApplications(ia).get(0);
				profileApplicationWrapper.setTaggedValue(sa, sa.eClass()
						.getEStructuralFeature("activated"), activated);
			}
		}
	}

	private void setTaggedValueForOutputArcs(Boolean activated, Place... places) {
		for (Place place : places) {
			for (OutputArc oa : getOutputArcs(place)) {
				StereotypeApplication sa = profileApplicationWrapper
						.getStereotypeApplications(oa).get(0);
				profileApplicationWrapper.setTaggedValue(sa, sa.eClass()
						.getEStructuralFeature("activated"), activated);
			}
		}
	}

	private void setTaggedValueForArcs(Boolean activated, Arc... arcs) {
		for (Arc arc : arcs) {
			StereotypeApplication sa = profileApplicationWrapper
					.getStereotypeApplications(arc).get(0);
			profileApplicationWrapper.setTaggedValue(sa, sa.eClass()
					.getEStructuralFeature("activated"), activated);
		}
	}

	private void removeStereotypeApplicationFromEObjects(EObject... eObjects) {
		for (EObject eObject : eObjects) {
			if (!profileApplicationWrapper.getStereotypeApplications(eObject)
					.isEmpty()) {
				profileApplicationWrapper
						.removeEObject(profileApplicationWrapper
								.getStereotypeApplications(eObject).get(0));
			}
		}
	}

	private void removeStereotypeApplicationFromInputArcsWithPlaces(
			Place... places) {
		for (Place place : places) {
			for (InputArc ia : getInputArcs(place)) {
				profileApplicationWrapper
						.removeEObject(profileApplicationWrapper
								.getStereotypeApplications(ia).get(0));
			}
		}
	}

	private void removeStereotypeApplicationFromOutputArcsWithPlaces(
			Place... places) {
		for (Place place : places) {
			for (OutputArc oa : getOutputArcs(place)) {
				if (!profileApplicationWrapper.getStereotypeApplications(oa)
						.isEmpty()) {
					profileApplicationWrapper
							.removeEObject(profileApplicationWrapper
									.getStereotypeApplications(oa).get(0));
				}
			}
		}
	}

	// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	private Place getPlace(String name) {
		TreeIterator<EObject> ti = EcoreUtil.getAllContents(petriNet
				.eContents());
		while (ti.hasNext()) {
			EObject next = ti.next();
			if (next instanceof Place && ((Place) next).getName().equals(name)) {
				return (Place) next;
			}
		}
		throw new NullPointerException("Could not find Place with name: "
				+ name);
	}

	private Transition getTransition(String name) {
		TreeIterator<EObject> ti = EcoreUtil.getAllContents(petriNet
				.eContents());
		while (ti.hasNext()) {
			EObject next = ti.next();
			if (next instanceof Transition
					&& ((Transition) next).getName().equals(name)) {
				return (Transition) next;
			}
		}
		throw new NullPointerException("Could not find Transition with name: "
				+ name);
	}

	private Collection<InputArc> getInputArcs(Place place) {
		List<InputArc> result = new ArrayList<>();
		TreeIterator<EObject> ti = EcoreUtil.getAllContents(petriNet
				.eContents());
		while (ti.hasNext()) {
			EObject next = ti.next();
			if (next instanceof InputArc) {
				if (((InputArc) next).getFrom().equals(place)) {
					result.add((InputArc) next);
				}
			}
		}
		return result;
	}

	private Collection<OutputArc> getOutputArcs(Place place) {
		List<OutputArc> result = new ArrayList<>();
		TreeIterator<EObject> ti = EcoreUtil.getAllContents(petriNet
				.eContents());
		while (ti.hasNext()) {
			EObject next = ti.next();
			if (next instanceof OutputArc) {
				if (((OutputArc) next).getTo().equals(place)) {
					result.add((OutputArc) next);
				}
			}
		}
		return result;
	}

	/**
	 * Returns the resolved {@link ResourceSet resource set} from the editing
	 * domain of the supplied <code>editorPart</code>.
	 * 
	 * @param editorPart
	 * @return the resource set.
	 * @throws NullPointerException
	 */
	public static final ResourceSet getResourceSet(IEditorPart editorPart)
			throws NullPointerException {
		Object adapter = editorPart.getAdapter(IEditingDomainProvider.class);
		if (adapter != null && adapter instanceof IEditingDomainProvider) {
			IEditingDomainProvider editingDomainProvider = (IEditingDomainProvider) adapter;
			if (editingDomainProvider.getEditingDomain() != null)
				return editingDomainProvider.getEditingDomain()
						.getResourceSet();
		}
		throw new NullPointerException(
				"This editor part does not poses an editing domain from which the resource set could be resolved.");
	}

	/**
	 * Gets an Editor Id from the provided {@link IWorkbenchPart workbench part}
	 * .
	 * 
	 * @param part
	 * @return
	 */
	public static final String getEditorIdFromEditorPart(IWorkbenchPart part) {
		return part.getSite().getId();
	}
	
	@Override
	public void dispose() {
		super.dispose();
		if(boldFont != null)
			boldFont.dispose();
	}
}