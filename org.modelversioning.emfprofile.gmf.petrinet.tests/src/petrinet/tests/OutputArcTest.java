/**
 */
package petrinet.tests;

import junit.textui.TestRunner;

import petrinet.OutputArc;
import petrinet.PetrinetFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Output Arc</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class OutputArcTest extends ArcTest
{

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static void main(String[] args)
  {
    TestRunner.run(OutputArcTest.class);
  }

  /**
   * Constructs a new Output Arc test case with the given name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public OutputArcTest(String name)
  {
    super(name);
  }

  /**
   * Returns the fixture for this Output Arc test case.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected OutputArc getFixture()
  {
    return (OutputArc)fixture;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see junit.framework.TestCase#setUp()
   * @generated
   */
  @Override
  protected void setUp() throws Exception
  {
    setFixture(PetrinetFactory.eINSTANCE.createOutputArc());
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see junit.framework.TestCase#tearDown()
   * @generated
   */
  @Override
  protected void tearDown() throws Exception
  {
    setFixture(null);
  }

} //OutputArcTest
