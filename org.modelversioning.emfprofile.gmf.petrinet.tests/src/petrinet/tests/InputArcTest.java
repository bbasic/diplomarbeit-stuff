/**
 */
package petrinet.tests;

import junit.textui.TestRunner;

import petrinet.InputArc;
import petrinet.PetrinetFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Input Arc</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class InputArcTest extends ArcTest
{

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static void main(String[] args)
  {
    TestRunner.run(InputArcTest.class);
  }

  /**
   * Constructs a new Input Arc test case with the given name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public InputArcTest(String name)
  {
    super(name);
  }

  /**
   * Returns the fixture for this Input Arc test case.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected InputArc getFixture()
  {
    return (InputArc)fixture;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see junit.framework.TestCase#setUp()
   * @generated
   */
  @Override
  protected void setUp() throws Exception
  {
    setFixture(PetrinetFactory.eINSTANCE.createInputArc());
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see junit.framework.TestCase#tearDown()
   * @generated
   */
  @Override
  protected void tearDown() throws Exception
  {
    setFixture(null);
  }

} //InputArcTest
