/**
 */
package petrinet.tests;

import petrinet.Arc;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Arc</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class ArcTest extends ElementTest
{

  /**
   * Constructs a new Arc test case with the given name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ArcTest(String name)
  {
    super(name);
  }

  /**
   * Returns the fixture for this Arc test case.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected Arc getFixture()
  {
    return (Arc)fixture;
  }

} //ArcTest
