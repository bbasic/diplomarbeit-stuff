# Advanced Model Decoration with EMF Profiles

##  Here are all other stuff from my master thesis which do not belong to the repository of the emf-profiles:

* **Petrinet** model execution simulator plug-in.
* GMF editor for Petrinet (relized with EuGENia)
* Graphiti editor for Petrinet (realized with **spray**)
