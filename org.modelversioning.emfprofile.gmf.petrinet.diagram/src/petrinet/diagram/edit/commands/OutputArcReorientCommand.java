/*
 * 
 */
package petrinet.diagram.edit.commands;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.emf.type.core.commands.EditElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.ReorientRelationshipRequest;

import petrinet.OutputArc;
import petrinet.PetriNet;
import petrinet.Place;
import petrinet.Transition;
import petrinet.diagram.edit.policies.PetrinetBaseItemSemanticEditPolicy;

/**
 * @generated
 */
public class OutputArcReorientCommand extends EditElementCommand {

	/**
	 * @generated
	 */
	private final int reorientDirection;

	/**
	 * @generated
	 */
	private final EObject oldEnd;

	/**
	 * @generated
	 */
	private final EObject newEnd;

	/**
	 * @generated
	 */
	public OutputArcReorientCommand(ReorientRelationshipRequest request) {
		super(request.getLabel(), request.getRelationship(), request);
		reorientDirection = request.getDirection();
		oldEnd = request.getOldRelationshipEnd();
		newEnd = request.getNewRelationshipEnd();
	}

	/**
	 * @generated
	 */
	public boolean canExecute() {
		if (false == getElementToEdit() instanceof OutputArc) {
			return false;
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_SOURCE) {
			return canReorientSource();
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_TARGET) {
			return canReorientTarget();
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected boolean canReorientSource() {
		if (!(oldEnd instanceof Transition && newEnd instanceof Transition)) {
			return false;
		}
		Place target = getLink().getTo();
		if (!(getLink().eContainer() instanceof PetriNet)) {
			return false;
		}
		PetriNet container = (PetriNet) getLink().eContainer();
		return PetrinetBaseItemSemanticEditPolicy.getLinkConstraints()
				.canExistOutputArc_4001(container, getLink(), getNewSource(),
						target);
	}

	/**
	 * @generated
	 */
	protected boolean canReorientTarget() {
		if (!(oldEnd instanceof Place && newEnd instanceof Place)) {
			return false;
		}
		Transition source = getLink().getFrom();
		if (!(getLink().eContainer() instanceof PetriNet)) {
			return false;
		}
		PetriNet container = (PetriNet) getLink().eContainer();
		return PetrinetBaseItemSemanticEditPolicy.getLinkConstraints()
				.canExistOutputArc_4001(container, getLink(), source,
						getNewTarget());
	}

	/**
	 * @generated
	 */
	protected CommandResult doExecuteWithResult(IProgressMonitor monitor,
			IAdaptable info) throws ExecutionException {
		if (!canExecute()) {
			throw new ExecutionException(
					"Invalid arguments in reorient link command"); //$NON-NLS-1$
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_SOURCE) {
			return reorientSource();
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_TARGET) {
			return reorientTarget();
		}
		throw new IllegalStateException();
	}

	/**
	 * @generated
	 */
	protected CommandResult reorientSource() throws ExecutionException {
		getLink().setFrom(getNewSource());
		return CommandResult.newOKCommandResult(getLink());
	}

	/**
	 * @generated
	 */
	protected CommandResult reorientTarget() throws ExecutionException {
		getLink().setTo(getNewTarget());
		return CommandResult.newOKCommandResult(getLink());
	}

	/**
	 * @generated
	 */
	protected OutputArc getLink() {
		return (OutputArc) getElementToEdit();
	}

	/**
	 * @generated
	 */
	protected Transition getOldSource() {
		return (Transition) oldEnd;
	}

	/**
	 * @generated
	 */
	protected Transition getNewSource() {
		return (Transition) newEnd;
	}

	/**
	 * @generated
	 */
	protected Place getOldTarget() {
		return (Place) oldEnd;
	}

	/**
	 * @generated
	 */
	protected Place getNewTarget() {
		return (Place) newEnd;
	}
}
