/*
 * 
 */
package petrinet.diagram.part;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.tooling.runtime.update.DiagramUpdater;

import petrinet.Element;
import petrinet.InputArc;
import petrinet.OutputArc;
import petrinet.PetriNet;
import petrinet.PetrinetPackage;
import petrinet.Place;
import petrinet.Transition;
import petrinet.diagram.edit.parts.InputArcEditPart;
import petrinet.diagram.edit.parts.OutputArcEditPart;
import petrinet.diagram.edit.parts.PetriNetEditPart;
import petrinet.diagram.edit.parts.PlaceEditPart;
import petrinet.diagram.edit.parts.TransitionEditPart;
import petrinet.diagram.providers.PetrinetElementTypes;

/**
 * @generated
 */
public class PetrinetDiagramUpdater {

	/**
	 * @generated
	 */
	public static boolean isShortcutOrphaned(View view) {
		return !view.isSetElement() || view.getElement() == null
				|| view.getElement().eIsProxy();
	}

	/**
	 * @generated
	 */
	public static List<PetrinetNodeDescriptor> getSemanticChildren(View view) {
		switch (PetrinetVisualIDRegistry.getVisualID(view)) {
		case PetriNetEditPart.VISUAL_ID:
			return getPetriNet_1000SemanticChildren(view);
		}
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<PetrinetNodeDescriptor> getPetriNet_1000SemanticChildren(
			View view) {
		if (!view.isSetElement()) {
			return Collections.emptyList();
		}
		PetriNet modelElement = (PetriNet) view.getElement();
		LinkedList<PetrinetNodeDescriptor> result = new LinkedList<PetrinetNodeDescriptor>();
		for (Iterator<?> it = modelElement.getElements().iterator(); it
				.hasNext();) {
			Element childElement = (Element) it.next();
			int visualID = PetrinetVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == TransitionEditPart.VISUAL_ID) {
				result.add(new PetrinetNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == PlaceEditPart.VISUAL_ID) {
				result.add(new PetrinetNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List<PetrinetLinkDescriptor> getContainedLinks(View view) {
		switch (PetrinetVisualIDRegistry.getVisualID(view)) {
		case PetriNetEditPart.VISUAL_ID:
			return getPetriNet_1000ContainedLinks(view);
		case TransitionEditPart.VISUAL_ID:
			return getTransition_2001ContainedLinks(view);
		case PlaceEditPart.VISUAL_ID:
			return getPlace_2002ContainedLinks(view);
		case OutputArcEditPart.VISUAL_ID:
			return getOutputArc_4001ContainedLinks(view);
		case InputArcEditPart.VISUAL_ID:
			return getInputArc_4002ContainedLinks(view);
		}
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<PetrinetLinkDescriptor> getIncomingLinks(View view) {
		switch (PetrinetVisualIDRegistry.getVisualID(view)) {
		case TransitionEditPart.VISUAL_ID:
			return getTransition_2001IncomingLinks(view);
		case PlaceEditPart.VISUAL_ID:
			return getPlace_2002IncomingLinks(view);
		case OutputArcEditPart.VISUAL_ID:
			return getOutputArc_4001IncomingLinks(view);
		case InputArcEditPart.VISUAL_ID:
			return getInputArc_4002IncomingLinks(view);
		}
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<PetrinetLinkDescriptor> getOutgoingLinks(View view) {
		switch (PetrinetVisualIDRegistry.getVisualID(view)) {
		case TransitionEditPart.VISUAL_ID:
			return getTransition_2001OutgoingLinks(view);
		case PlaceEditPart.VISUAL_ID:
			return getPlace_2002OutgoingLinks(view);
		case OutputArcEditPart.VISUAL_ID:
			return getOutputArc_4001OutgoingLinks(view);
		case InputArcEditPart.VISUAL_ID:
			return getInputArc_4002OutgoingLinks(view);
		}
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<PetrinetLinkDescriptor> getPetriNet_1000ContainedLinks(
			View view) {
		PetriNet modelElement = (PetriNet) view.getElement();
		LinkedList<PetrinetLinkDescriptor> result = new LinkedList<PetrinetLinkDescriptor>();
		result.addAll(getContainedTypeModelFacetLinks_OutputArc_4001(modelElement));
		result.addAll(getContainedTypeModelFacetLinks_InputArc_4002(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<PetrinetLinkDescriptor> getTransition_2001ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<PetrinetLinkDescriptor> getPlace_2002ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<PetrinetLinkDescriptor> getOutputArc_4001ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<PetrinetLinkDescriptor> getInputArc_4002ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<PetrinetLinkDescriptor> getTransition_2001IncomingLinks(
			View view) {
		Transition modelElement = (Transition) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<PetrinetLinkDescriptor> result = new LinkedList<PetrinetLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_InputArc_4002(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<PetrinetLinkDescriptor> getPlace_2002IncomingLinks(
			View view) {
		Place modelElement = (Place) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<PetrinetLinkDescriptor> result = new LinkedList<PetrinetLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_OutputArc_4001(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<PetrinetLinkDescriptor> getOutputArc_4001IncomingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<PetrinetLinkDescriptor> getInputArc_4002IncomingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<PetrinetLinkDescriptor> getTransition_2001OutgoingLinks(
			View view) {
		Transition modelElement = (Transition) view.getElement();
		LinkedList<PetrinetLinkDescriptor> result = new LinkedList<PetrinetLinkDescriptor>();
		result.addAll(getOutgoingTypeModelFacetLinks_OutputArc_4001(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<PetrinetLinkDescriptor> getPlace_2002OutgoingLinks(
			View view) {
		Place modelElement = (Place) view.getElement();
		LinkedList<PetrinetLinkDescriptor> result = new LinkedList<PetrinetLinkDescriptor>();
		result.addAll(getOutgoingTypeModelFacetLinks_InputArc_4002(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<PetrinetLinkDescriptor> getOutputArc_4001OutgoingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<PetrinetLinkDescriptor> getInputArc_4002OutgoingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	private static Collection<PetrinetLinkDescriptor> getContainedTypeModelFacetLinks_OutputArc_4001(
			PetriNet container) {
		LinkedList<PetrinetLinkDescriptor> result = new LinkedList<PetrinetLinkDescriptor>();
		for (Iterator<?> links = container.getElements().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof OutputArc) {
				continue;
			}
			OutputArc link = (OutputArc) linkObject;
			if (OutputArcEditPart.VISUAL_ID != PetrinetVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			Place dst = link.getTo();
			Transition src = link.getFrom();
			result.add(new PetrinetLinkDescriptor(src, dst, link,
					PetrinetElementTypes.OutputArc_4001,
					OutputArcEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<PetrinetLinkDescriptor> getContainedTypeModelFacetLinks_InputArc_4002(
			PetriNet container) {
		LinkedList<PetrinetLinkDescriptor> result = new LinkedList<PetrinetLinkDescriptor>();
		for (Iterator<?> links = container.getElements().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof InputArc) {
				continue;
			}
			InputArc link = (InputArc) linkObject;
			if (InputArcEditPart.VISUAL_ID != PetrinetVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			Transition dst = link.getTo();
			Place src = link.getFrom();
			result.add(new PetrinetLinkDescriptor(src, dst, link,
					PetrinetElementTypes.InputArc_4002,
					InputArcEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<PetrinetLinkDescriptor> getIncomingTypeModelFacetLinks_OutputArc_4001(
			Place target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<PetrinetLinkDescriptor> result = new LinkedList<PetrinetLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences
				.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() != PetrinetPackage.eINSTANCE
					.getOutputArc_To()
					|| false == setting.getEObject() instanceof OutputArc) {
				continue;
			}
			OutputArc link = (OutputArc) setting.getEObject();
			if (OutputArcEditPart.VISUAL_ID != PetrinetVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			Transition src = link.getFrom();
			result.add(new PetrinetLinkDescriptor(src, target, link,
					PetrinetElementTypes.OutputArc_4001,
					OutputArcEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<PetrinetLinkDescriptor> getIncomingTypeModelFacetLinks_InputArc_4002(
			Transition target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<PetrinetLinkDescriptor> result = new LinkedList<PetrinetLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences
				.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() != PetrinetPackage.eINSTANCE
					.getInputArc_To()
					|| false == setting.getEObject() instanceof InputArc) {
				continue;
			}
			InputArc link = (InputArc) setting.getEObject();
			if (InputArcEditPart.VISUAL_ID != PetrinetVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			Place src = link.getFrom();
			result.add(new PetrinetLinkDescriptor(src, target, link,
					PetrinetElementTypes.InputArc_4002,
					InputArcEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<PetrinetLinkDescriptor> getOutgoingTypeModelFacetLinks_OutputArc_4001(
			Transition source) {
		PetriNet container = null;
		// Find container element for the link.
		// Climb up by containment hierarchy starting from the source
		// and return the first element that is instance of the container class.
		for (EObject element = source; element != null && container == null; element = element
				.eContainer()) {
			if (element instanceof PetriNet) {
				container = (PetriNet) element;
			}
		}
		if (container == null) {
			return Collections.emptyList();
		}
		LinkedList<PetrinetLinkDescriptor> result = new LinkedList<PetrinetLinkDescriptor>();
		for (Iterator<?> links = container.getElements().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof OutputArc) {
				continue;
			}
			OutputArc link = (OutputArc) linkObject;
			if (OutputArcEditPart.VISUAL_ID != PetrinetVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			Place dst = link.getTo();
			Transition src = link.getFrom();
			if (src != source) {
				continue;
			}
			result.add(new PetrinetLinkDescriptor(src, dst, link,
					PetrinetElementTypes.OutputArc_4001,
					OutputArcEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<PetrinetLinkDescriptor> getOutgoingTypeModelFacetLinks_InputArc_4002(
			Place source) {
		PetriNet container = null;
		// Find container element for the link.
		// Climb up by containment hierarchy starting from the source
		// and return the first element that is instance of the container class.
		for (EObject element = source; element != null && container == null; element = element
				.eContainer()) {
			if (element instanceof PetriNet) {
				container = (PetriNet) element;
			}
		}
		if (container == null) {
			return Collections.emptyList();
		}
		LinkedList<PetrinetLinkDescriptor> result = new LinkedList<PetrinetLinkDescriptor>();
		for (Iterator<?> links = container.getElements().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof InputArc) {
				continue;
			}
			InputArc link = (InputArc) linkObject;
			if (InputArcEditPart.VISUAL_ID != PetrinetVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			Transition dst = link.getTo();
			Place src = link.getFrom();
			if (src != source) {
				continue;
			}
			result.add(new PetrinetLinkDescriptor(src, dst, link,
					PetrinetElementTypes.InputArc_4002,
					InputArcEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static final DiagramUpdater TYPED_INSTANCE = new DiagramUpdater() {
		/**
		 * @generated
		 */
		@Override
		public List<PetrinetNodeDescriptor> getSemanticChildren(View view) {
			return PetrinetDiagramUpdater.getSemanticChildren(view);
		}

		/**
		 * @generated
		 */
		@Override
		public List<PetrinetLinkDescriptor> getContainedLinks(View view) {
			return PetrinetDiagramUpdater.getContainedLinks(view);
		}

		/**
		 * @generated
		 */
		@Override
		public List<PetrinetLinkDescriptor> getIncomingLinks(View view) {
			return PetrinetDiagramUpdater.getIncomingLinks(view);
		}

		/**
		 * @generated
		 */
		@Override
		public List<PetrinetLinkDescriptor> getOutgoingLinks(View view) {
			return PetrinetDiagramUpdater.getOutgoingLinks(view);
		}
	};

}
