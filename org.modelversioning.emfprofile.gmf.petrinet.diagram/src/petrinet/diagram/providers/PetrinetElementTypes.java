/*
 * 
 */
package petrinet.diagram.providers;

import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.ENamedElement;
import org.eclipse.gmf.runtime.emf.type.core.ElementTypeRegistry;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.tooling.runtime.providers.DiagramElementTypeImages;
import org.eclipse.gmf.tooling.runtime.providers.DiagramElementTypes;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;

import petrinet.PetrinetPackage;
import petrinet.diagram.edit.parts.InputArcEditPart;
import petrinet.diagram.edit.parts.OutputArcEditPart;
import petrinet.diagram.edit.parts.PetriNetEditPart;
import petrinet.diagram.edit.parts.PlaceEditPart;
import petrinet.diagram.edit.parts.TransitionEditPart;
import petrinet.diagram.part.PetrinetDiagramEditorPlugin;

/**
 * @generated
 */
public class PetrinetElementTypes {

	/**
	 * @generated
	 */
	private PetrinetElementTypes() {
	}

	/**
	 * @generated
	 */
	private static Map<IElementType, ENamedElement> elements;

	/**
	 * @generated
	 */
	private static DiagramElementTypeImages elementTypeImages = new DiagramElementTypeImages(
			PetrinetDiagramEditorPlugin.getInstance()
					.getItemProvidersAdapterFactory());

	/**
	 * @generated
	 */
	private static Set<IElementType> KNOWN_ELEMENT_TYPES;

	/**
	 * @generated
	 */
	public static final IElementType PetriNet_1000 = getElementType("org.modelversioning.emfprofile.gmf.petrinet.diagram.PetriNet_1000"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType Transition_2001 = getElementType("org.modelversioning.emfprofile.gmf.petrinet.diagram.Transition_2001"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType Place_2002 = getElementType("org.modelversioning.emfprofile.gmf.petrinet.diagram.Place_2002"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType OutputArc_4001 = getElementType("org.modelversioning.emfprofile.gmf.petrinet.diagram.OutputArc_4001"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType InputArc_4002 = getElementType("org.modelversioning.emfprofile.gmf.petrinet.diagram.InputArc_4002"); //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static ImageDescriptor getImageDescriptor(ENamedElement element) {
		return elementTypeImages.getImageDescriptor(element);
	}

	/**
	 * @generated
	 */
	public static Image getImage(ENamedElement element) {
		return elementTypeImages.getImage(element);
	}

	/**
	 * @generated
	 */
	public static ImageDescriptor getImageDescriptor(IAdaptable hint) {
		return getImageDescriptor(getElement(hint));
	}

	/**
	 * @generated
	 */
	public static Image getImage(IAdaptable hint) {
		return getImage(getElement(hint));
	}

	/**
	 * Returns 'type' of the ecore object associated with the hint.
	 * 
	 * @generated
	 */
	public static ENamedElement getElement(IAdaptable hint) {
		Object type = hint.getAdapter(IElementType.class);
		if (elements == null) {
			elements = new IdentityHashMap<IElementType, ENamedElement>();

			elements.put(PetriNet_1000, PetrinetPackage.eINSTANCE.getPetriNet());

			elements.put(Transition_2001,
					PetrinetPackage.eINSTANCE.getTransition());

			elements.put(Place_2002, PetrinetPackage.eINSTANCE.getPlace());

			elements.put(OutputArc_4001,
					PetrinetPackage.eINSTANCE.getOutputArc());

			elements.put(InputArc_4002, PetrinetPackage.eINSTANCE.getInputArc());
		}
		return (ENamedElement) elements.get(type);
	}

	/**
	 * @generated
	 */
	private static IElementType getElementType(String id) {
		return ElementTypeRegistry.getInstance().getType(id);
	}

	/**
	 * @generated
	 */
	public static boolean isKnownElementType(IElementType elementType) {
		if (KNOWN_ELEMENT_TYPES == null) {
			KNOWN_ELEMENT_TYPES = new HashSet<IElementType>();
			KNOWN_ELEMENT_TYPES.add(PetriNet_1000);
			KNOWN_ELEMENT_TYPES.add(Transition_2001);
			KNOWN_ELEMENT_TYPES.add(Place_2002);
			KNOWN_ELEMENT_TYPES.add(OutputArc_4001);
			KNOWN_ELEMENT_TYPES.add(InputArc_4002);
		}
		return KNOWN_ELEMENT_TYPES.contains(elementType);
	}

	/**
	 * @generated
	 */
	public static IElementType getElementType(int visualID) {
		switch (visualID) {
		case PetriNetEditPart.VISUAL_ID:
			return PetriNet_1000;
		case TransitionEditPart.VISUAL_ID:
			return Transition_2001;
		case PlaceEditPart.VISUAL_ID:
			return Place_2002;
		case OutputArcEditPart.VISUAL_ID:
			return OutputArc_4001;
		case InputArcEditPart.VISUAL_ID:
			return InputArc_4002;
		}
		return null;
	}

	/**
	 * @generated
	 */
	public static final DiagramElementTypes TYPED_INSTANCE = new DiagramElementTypes(
			elementTypeImages) {

		/**
		 * @generated
		 */
		@Override
		public boolean isKnownElementType(IElementType elementType) {
			return petrinet.diagram.providers.PetrinetElementTypes
					.isKnownElementType(elementType);
		}

		/**
		 * @generated
		 */
		@Override
		public IElementType getElementTypeForVisualId(int visualID) {
			return petrinet.diagram.providers.PetrinetElementTypes
					.getElementType(visualID);
		}

		/**
		 * @generated
		 */
		@Override
		public ENamedElement getDefiningNamedElement(
				IAdaptable elementTypeAdapter) {
			return petrinet.diagram.providers.PetrinetElementTypes
					.getElement(elementTypeAdapter);
		}
	};

}
